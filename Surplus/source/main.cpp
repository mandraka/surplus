#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include "Engine/Menus/MainMenu.h"
#include "Engine/Menus/CreateMapMenu.h"
#include "Engine/Menus/NewGameMenu.h"
#include "Engine/Menus/LoadGameMenu.h"
#include "Engine/Menus/OptionsMenu.h"
#include "Engine/Game/Game.h"
#include "Engine/Game/MapEditor.h"
#include "Fields/nationality.h"
#include "stance.h"
#include "Engine/Map.h"
#include "Engine/Save.h"

int main()
{
	Map* map;
	map = new Map();
	Save save;

	//audio
	sf::SoundBuffer clickBuffer;
	sf::Sound clickSound;
	if (!clickBuffer.loadFromFile("sounds/click.wav"))
		std::cout << "Can't load click sound";
	clickSound.setBuffer(clickBuffer);
	//
	nationality player;
	sf::ContextSettings settings;
	//settings.antialiasingLevel = 8;
	sf::RenderWindow window(sf::VideoMode(1980, 1050, 32), "Surplus", sf::Style::Titlebar | sf::Style::Close, settings); //| sf::Style::Fullscreen
	window.setVerticalSyncEnabled(true);

	stance mainStance = sMainMenu;
	while (mainStance != sExit)
		switch (mainStance)
		{
		case sGame:
		{
			Game game(&window, &mainStance, &save, player);
			game.mainLoop();
			delete save.map;
		}
		break;
		case sMapEdit:
		{
			MapEditor mapEditor(&window, &mainStance, map);
			mapEditor.mainLoop();
			delete map;
			map = new Map();
		}
		break;
		case sMainMenu:
		{
			MainMenu mainMenu(&window, &mainStance, &clickSound);
			mainMenu.mainLoop();
		}
		break;
		case sCreateMapMenu:
		{
			CreateMapMenu createMapMenu(&window, &mainStance, map, &clickSound);
			createMapMenu.mainLoop();
		}
		break;
		case sNewGameMenu:
		{
			NewGameMenu newGameMenu(&window, &mainStance, map, &save, &player, &clickSound);
			newGameMenu.mainLoop();
		}
		break;
		case sLoadGameMenu:
		{
			LoadGameMenu loadGameMenu(&window, &mainStance, &save, &clickSound);
			loadGameMenu.mainLoop();
		}
		break;
		case sOptions:
		{
			OptionsMenu optionsMenu(&window, &mainStance, &clickSound);
			optionsMenu.mainLoop();
		}
		break;
		default:
			window.close();
			return 1;
		}
	window.close();
	return 0;
}