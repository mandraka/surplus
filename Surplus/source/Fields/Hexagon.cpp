#include "Hexagon.h"
#include "Factory.h"
#include "nationality.h"

Hexagon::Hexagon()
{
	visible = false;
	selected = false;
	wasAStarred = false;
	factory = nullptr;
	infrastructureLvl = 0;
	supplyDepotLvl = 0;
	townLvl = 0;
	buildingProgress = -1;
	btarg = bnull;
	selectedLogUnit = -1;
	selectedMilUnit = -1;
	Resource r = Resource();
	//	for (int i = 1; i <= r.getResourceAmmount()+1; i++)
	//		depotResources.push_back(Resource((TypeOfResource)i, 0));
	calculateWeight();
}

Hexagon::Hexagon(int posX, int posY) : Hexagon()
{
	this->pos = sf::Vector2i(posX, posY);
}

Hexagon::~Hexagon()
{
}

void Hexagon::setPos(int posX, int posY)
{
	this->pos = sf::Vector2i(posX, posY);
}

sf::Vector2i Hexagon::getPos()
{
	return pos;
}

void Hexagon::setNationality(nationality n)
{
	nation = n;
}

nationality Hexagon::getNationality()
{
	return this->nation;
}

void Hexagon::setTerrain(terrainType t)
{
	terrain = t;
	if (t == plains)
	{
		speedMultiplier = 1;
		supplyLimit = 5;
		passable = true;
	}
	else if (t == forests)
	{
		speedMultiplier = 0.667f;
		supplyLimit = 3;
		passable = true;
	}
	else if (t == mountains)
	{
		speedMultiplier = 0.5f;
		supplyLimit = 2;
		passable = true;
	}
	else if (t == hills)
	{
		speedMultiplier = 0.667f;
		supplyLimit = 3;
		passable = true;
	}
	else if (t == marshes)
	{
		speedMultiplier = 0.5f;
		supplyLimit = 2;
		passable = true;
	}
	else if (t == water)
	{
		speedMultiplier = 0;
		supplyLimit = 0;
		passable = false;
	}
	else if (t == noTerrain)
	{
		speedMultiplier = 0;
		supplyLimit = 0;
		passable = false;
	}
}

terrainType Hexagon::getTerrain()
{
	return this->terrain;
}

bool Hexagon::switchSelection()
{
	selected = !selected;
	return selected;
}

bool Hexagon::changeResource(TypeOfResource typ, int ammount)
{
	switch (typ)
	{
	case iron:
		ironAmmount += ammount;
		break;
	case food:
		foodAmmount += ammount;
		break;
	case coal:
		coalAmmount += ammount;
		break;
	case wood:
		woodAmmount += ammount;
		break;
	case steel:
		steelAmmount += ammount;
		break;
	case supply:
		supplyAmmount += ammount;
		break;
	case manpower:
		manpowerAmmount += ammount;
		break;
	default:
		break;
	}
	calculateWeight();
	if (totalWeight > supplyDepotLvl * 10000+10000)
	{
		switch (typ)
		{
		case iron:
			ironAmmount -= ammount;
			break;
		case food:
			foodAmmount -= ammount;
			break;
		case coal:
			coalAmmount -= ammount;
			break;
		case wood:
			woodAmmount -= ammount;
			break;
		case steel:
			steelAmmount -= ammount;
			break;
		case supply:
			supplyAmmount -= ammount;
			break;
		case manpower:
			manpowerAmmount -= ammount;
			break;
		default:
			break;
		}
		return 0;
	}
	else return 1;
}

int Hexagon::getResource(TypeOfResource typ)
{
	switch (typ)
	{
	case iron:
		return ironAmmount;
		break;
	case food:
		return foodAmmount;
		break;
	case coal:
		return coalAmmount;
		break;
	case wood:
		return woodAmmount;
		break;
	case steel:
		return steelAmmount;
		break;
	case supply:
		return supplyAmmount;
		break;
	case manpower:
		return manpowerAmmount;
		break;
	default:
		return 0;
	}
}

void Hexagon::calculateWeight()
{
	totalWeight = 0;
	totalWeight += ironAmmount * ResourceWeights[iron];
	totalWeight += foodAmmount * ResourceWeights[food];
	totalWeight += coalAmmount * ResourceWeights[coal];
	totalWeight += woodAmmount * ResourceWeights[wood];
	totalWeight += steelAmmount * ResourceWeights[steel];
	totalWeight += supplyAmmount * ResourceWeights[supply];
	totalWeight += manpowerAmmount * ResourceWeights[manpower];
}

void Hexagon::setInfrastructureLvl(int l)
{
	this->infrastructureLvl = l;
	this->speedMultiplier += speedMultiplier * l*0.25f;
}

void Hexagon::setSupplyDepotLvl(int l)
{
	this->supplyDepotLvl = l;
}

void Hexagon::setTownLvl(int l)
{
	this->townLvl = l;
}

void Hexagon::step()
{
	if (this->factory != nullptr && factory->isUsable())
		factory->process();
	if (buildingProgress == 0)
	{
		//take resources accordingly
		switch (btarg)
		{
		case bnull:
			buildingProgress = -1;
			break;
		case binfrastructure:
			break;
		case bsupplyDepot:
			break;
		case bcity:
			break;
		case bfactory:
			break;
		case bmilUnit:
			break;
		case blogUnit:
			break;
		default:
			buildingProgress = -1;
			btarg = bnull;
			break;
		}
	}
	else if (buildingProgress > 0)
	{
		buildingProgress++;
		switch (btarg)
		{
		case bnull:
			buildingProgress = -1;
			break;
		case binfrastructure:
			if (buildingProgress > 100)
			{
				infrastructureLvl++;
				buildingProgress = -1;
				btarg = bnull;
			}
			break;
		case bsupplyDepot:
			if (buildingProgress > 100)
			{
				supplyDepotLvl++;
				buildingProgress = -1;
				btarg = bnull;
			}
			break;
		case bcity:
			if (buildingProgress > 100)
			{
				townLvl++;
				buildingProgress = -1;
				btarg = bnull;
			}
			break;
		case bfactory:
			if (buildingProgress > factory->getBuildingTime())
			{
				factory->setUsable(true);
				factory->setLevel(factory->getLevel() + 1);
				buildingProgress = -1;
				btarg = bnull;
			}
			break;
		case bmilUnit:
			if (buildingProgress > 100)
			{
				buildingProgress = -1;
				btarg = bnull;
			}
			break;
		case blogUnit:
			if (buildingProgress > 100)
			{

				buildingProgress = -1;
				btarg = bnull;
			}
			break;
		default:
			buildingProgress = -1;
			btarg = bnull;
			break;
		}

	}
	for (int i = 0; i < militaryUnits.size(); i++)
		militaryUnits[i].step();
	for (int i = 0; i < logisticUnits.size(); i++)
		logisticUnits[i].step();

}