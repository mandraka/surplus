#pragma once
#include <string>
#include <vector>
const int ResourceWeights[8] = { 0,     2,    1,   2,    1  ,   2 ,    1 ,     1 };
enum TypeOfResource : short { null, iron, food, coal, wood, steel, supply, manpower };
const std::string resource_str[8] = { "Null", "Iron", "Food", "Coal", "Wood", "Steel", "Supply", "Manpower" };

struct Resource
{
private:
	TypeOfResource type;
	int weight;
	int ammount;
	void refreshWeight();
public:
	
	bool isSource;
	Resource();
	Resource(TypeOfResource, int ammount);
	~Resource();
	Resource & operator = (const Resource &);
	TypeOfResource getType();
	std::vector<std::string> getResourceTypes();
	int getResourceAmmount();
	void setType(TypeOfResource);
	void setAmmount(int);
	void changeAmmount(int);
	int getAmmount();
	std::string getString();
};
