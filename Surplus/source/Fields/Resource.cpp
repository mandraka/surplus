#include "Resource.h"

Resource::Resource()
{
	this->type = null;
	weight = -1;
	ammount = -1;
}
Resource::Resource(TypeOfResource r, int ammount)
{
	this->type = r;
	if (ammount >= 0)
	{
		this->ammount = ammount;
		isSource = false;
		refreshWeight();
	}
	else if (ammount == -1)
		isSource = true;
}

Resource::~Resource()
{
}

Resource & Resource::operator=(const Resource &other)
{
	if (&other == this)
		return *this;
	this->type = other.type;
	this->ammount = other.ammount;
	this->weight = other.weight;
	this->isSource = other.isSource;
	return *this;
}

TypeOfResource Resource::getType()
{
	return this->type;
}

std::vector<std::string> Resource::getResourceTypes()
{
	std::vector<std::string> resources;

	for (int i = 1; i < sizeof(resource_str) / sizeof(resource_str[0]); i++)
	{
		resources.push_back(resource_str[i]);
	}
	return resources;
}

int Resource::getResourceAmmount()
{
	return sizeof(resource_str) / sizeof(resource_str[0]) - 1;
}

void Resource::setType(TypeOfResource t)
{
	this->type = t;
	if (ammount > 0)
	{
		refreshWeight();
		isSource = false;
	}
	else if (ammount == -1)
		isSource = true;
}

void Resource::setAmmount(int a)
{
	this->ammount = a;
	if (type!=null && ammount>=0)
		refreshWeight();
}

void Resource::changeAmmount(int val)
{
	this->ammount += val;
	refreshWeight();
}

int Resource::getAmmount()
{
	return ammount;
}

std::string Resource::getString()
{
	if (ammount > 0)
		return resource_str[type] + " " + std::to_string(ammount);
	else
	return resource_str[type];
}

void Resource::refreshWeight()
{
	if (type == coal)
		weight = ammount * 5;
	else if (type == food)
		weight = ammount * 5;
	else if (type == iron)
		weight = ammount * 5;
	else if (type == steel)
		weight = ammount * 5;
	else if (type == wood)
		weight = ammount * 5;
	else if (type == supply)
		weight = ammount * 5;
	else if (type == manpower)
		weight = ammount * 5;
}