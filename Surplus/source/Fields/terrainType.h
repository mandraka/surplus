#pragma once

enum terrainType {noTerrain, plains, forests, mountains, hills, marshes,  water };
const std::string terrainType_str[] = { "Null", "Fields", "Forests", "Mountains", "Hills", "Marshes", "Water" };