#pragma once
#include <vector>
#include <string>

struct Resource;
enum TypeOfResource : short;

class Factory
{
private:
	std::vector<Resource*> from;
	std::vector<Resource*> to;
	bool working;
	bool usable;
	int buildingTime;
	int rate;
	int level;
	int speed;
	
public:

	Factory(TypeOfResource);
	Factory(TypeOfResource, TypeOfResource);
	Factory(TypeOfResource, TypeOfResource, TypeOfResource);
	~Factory();
	void process();
	int getBuildingTime();
	bool isUsable();
	std::string showLevel();
	void setUsable(bool);
	std::string getInfo();
	void setLevel(int);
	int getLevel();

};

