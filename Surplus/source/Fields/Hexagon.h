#pragma once
#include <vector>
#include <SFML/System/Vector2.hpp>
#include "Resource.h"
#include "nationality.h"
#include "terrainType.h"
#include "../Units/MilitaryUnit.h"
#include "../Units/LogisticUnit.h"

class Factory;

enum buildTarget{bnull, binfrastructure, bsupplyDepot, bcity, bfactory, bmilUnit, blogUnit};
class Hexagon
{
private:
	terrainType terrain;
	nationality nation;
	unsigned short foodAmmount = 0;
	unsigned short woodAmmount = 0;
	unsigned short coalAmmount = 0;
	unsigned short ironAmmount = 0;
	unsigned short steelAmmount = 0;
	unsigned short supplyAmmount = 0;
	unsigned short manpowerAmmount = 0;
	unsigned short totalWeight = 0;
public:
	sf::Vector2i pos;
	bool selected;
	bool visible;
	bool passable;
	bool wasAStarred;
	float speedMultiplier;
	int supplyLimit;
	buildTarget btarg;
	int infrastructureLvl;
	int supplyDepotLvl;
	//std::vector<Resource> depotResources;
	int townLvl;
	int buildingProgress;
	//Resource *manpower;
	Resource resource;
	Factory *factory;
	std::vector<LogisticUnit> logisticUnits;
	int selectedLogUnit;
	std::vector<MilitaryUnit> militaryUnits;
	int selectedMilUnit;


	Hexagon();
	Hexagon(int, int);
	~Hexagon();
	void setPos(int, int);
	sf::Vector2i getPos();
	void setNationality(nationality);
	nationality getNationality();
	void setTerrain(terrainType);
	terrainType getTerrain();
	void setInfrastructureLvl(int);
	void setSupplyDepotLvl(int);
	void setTownLvl(int);
	bool switchSelection();
	bool changeResource(TypeOfResource, int);
	int getResource(TypeOfResource);
	void calculateWeight();
	void step();
};

