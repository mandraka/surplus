#include "Factory.h"
#include "Resource.h"
#include <string>

Factory::Factory(TypeOfResource t)
{
	working = false;
	if (t == coal)
	{
		buildingTime = 200;
	}
	if (t == iron)
	{
		buildingTime = 300;
	}
	to.push_back(new Resource(t, -1));
}
Factory::Factory(TypeOfResource f, TypeOfResource t)
{
	working = false;
	if (f == iron && t == steel)
	{
		buildingTime = 400;
	}
	from.push_back(new Resource(t, -1));
	to.push_back(new Resource(t, 0));

}

Factory::Factory(TypeOfResource f1, TypeOfResource f2, TypeOfResource t1)
{
}

Factory::~Factory()
{
}

void Factory::process()
{
	if (true)
	{
		working = true;

	}

}

int Factory::getBuildingTime()
{
	return buildingTime;
}

bool Factory::isUsable()
{
	return usable;
}

std::string Factory::showLevel()
{
	if (this != nullptr)
		return std::to_string(level);
	else
		return "0";
}

void Factory::setUsable(bool b)
{
	usable = b;
}

std::string Factory::getInfo()
{
	std::string output = "Factory lvl: " + this->getLevel();
	for (int i = 0; i < from.size(); i++)
		output += " From resource: " + from[i]->getType();
	for (int i = 0; i < to.size(); i++)
		output += " To resource: " + to[i]->getType();

	return output;
}

void Factory::setLevel(int lvl)
{
	level = lvl;
}

int Factory::getLevel()
{
	return level;
}
