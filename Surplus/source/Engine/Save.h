#pragma once
#include "Map.h"


class Save
{
private:
	bool empty;
public:
	Map* map;
	Save();
	Save(Map*);
	~Save();
	bool isEmpty();
	char* serialize();
	void deserialize(char*, Save*);
};

