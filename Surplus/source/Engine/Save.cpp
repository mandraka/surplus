#include "Save.h"


Save::Save()
{
	empty = true;
}
Save::Save(Map* map)
{
	empty = false;
	this->map = map;
}

Save::~Save()
{
}

bool Save::isEmpty()
{
	return empty;
}

char* Save::serialize()
{
	char* data = this->map->serialize();
	return data;
}

void Save::deserialize(char*, Save*)
{

}
