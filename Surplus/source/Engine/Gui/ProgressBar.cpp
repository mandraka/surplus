#include "ProgressBar.h"

namespace gui
{
	ProgressBar::ProgressBar()
	{
		back.setSize(sf::Vector2f(300.f, 80.f));
		back.setPosition(sf::Vector2f(100.f, 100.f));
		back.setFillColor(sf::Color(80, 80, 80, 80));
		bar.setSize(sf::Vector2f(0.f, 80.f));
		bar.setPosition(back.getPosition());
		bar.setFillColor(sf::Color(255, 0, 0, 145));
	}

	ProgressBar::ProgressBar(sf::Vector2f pos)
	{
		back.setSize(sf::Vector2f(300.f, 80.f));
		back.setPosition(pos);
		bar.setSize(sf::Vector2f(0.f, 80.f));
		bar.setPosition(back.getPosition());
		bar.setFillColor(sf::Color::Red);
	}

	ProgressBar::ProgressBar(sf::Vector2f pos, sf::Vector2f size)
	{
		back.setSize(size);
		back.setPosition(pos);
		bar.setSize(sf::Vector2f(0.f, size.y));
		bar.setPosition(back.getPosition());
		bar.setFillColor(sf::Color::Red);
	}

	ProgressBar::~ProgressBar()
	{
	}

	void ProgressBar::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(back, states);
		target.draw(bar, states);
	}

	void ProgressBar::setPos(sf::Vector2f pos)
	{
		back.setPosition(pos);
		bar.setPosition(back.getPosition());
	}

	void ProgressBar::setSize(sf::Vector2f size)
	{
		back.setSize(size);
		bar.setSize(sf::Vector2f(0.f, size.y));
	}

	void ProgressBar::setRotation(float angle)
	{
		back.setRotation(angle);
		bar.setRotation(angle);
	}

	void ProgressBar::setProgress(int progress)
	{
		if (progress <= 100 && progress >= 0)
		{
			float step = back.getSize().x / 100;
			bar.setSize(sf::Vector2f(step*progress, back.getSize().y));
			this->progress = progress;
		}
	}

	void ProgressBar::setScale(sf::Vector2f scale)
	{
		back.setScale(scale);
		bar.setScale(scale);
	}

	void ProgressBar::translate(sf::Vector2f pos)
	{
		setPos(sf::Vector2f(back.getPosition().x + pos.x, back.getPosition().y + pos.y));
	}

	int ProgressBar::getProgress()
	{
		return progress;
	}

}