#pragma once
#include <SFML/Graphics.hpp>

namespace gui
{
	//enum barMode {mUnitMoveBarMode, lUnitMoveBarMode, buildingBarMode, attackBarMode};
	class ProgressBar : public sf::Drawable
	{
	private:
		sf::RectangleShape back;
		sf::RectangleShape bar;
		int progress = 0;
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	public:
		ProgressBar();
		ProgressBar(sf::Vector2f);
		ProgressBar(sf::Vector2f, sf::Vector2f);
	//	ProgressBar(enum barMode, sf::Vector2f pos,  float angle 45 90 135 225 270 315);
		~ProgressBar();
		void setPos(sf::Vector2f);
		void setSize(sf::Vector2f);
		void setRotation(float);
		void setProgress(int); //0-100
		void setScale(sf::Vector2f); //0-100
		void translate(sf::Vector2f);
		int getProgress();
	};
}

