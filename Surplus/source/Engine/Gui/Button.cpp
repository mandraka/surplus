#include "Button.h"
#include <iostream>

namespace gui
{
	Button::Button()
	{
		if (!font.loadFromFile("fonts/OpenSans-Regular.ttf"))
			std::cout << "Can't load font";
		back.setSize(sf::Vector2f(300.f, 80.f));
		back.setPosition(sf::Vector2f(100.f, 100.f));
		background.setFillColor(sf::Color::Black);
		background.setSize(sf::Vector2f(back.getSize().x + 4.f, back.getSize().y + 4.f));
		background.setPosition(sf::Vector2f(back.getPosition().x - 2.f, back.getPosition().y - 2.f));
		text.setFont(font);
		text.setCharacterSize(24);
		text.setFillColor(sf::Color::Black);
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}
	Button::Button(const char* input) : Button()
	{
		text.setString(input);
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}
	Button::Button(const char* input, sf::Vector2f pos) : Button (input)
	{
		back.setPosition(pos);
		background.setPosition(sf::Vector2f(back.getPosition().x - 2.f, back.getPosition().y - 2.f));
		text.setString(input);
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}
	Button::Button(const char* input, sf::Vector2f pos, sf::Vector2f size, sf::Sound* clickSound) : Button (input, pos)
	{
		this->clickSound = clickSound;
		back.setSize(size);
		background.setSize(sf::Vector2f(back.getSize().x + 4.f, back.getSize().y + 4.f));
		back.setPosition(pos);
		background.setPosition(sf::Vector2f(back.getPosition().x - 1.f, back.getPosition().y - 1.f));
		text.setString(input);
		text.setCharacterSize(14 + unsigned(size.x) / 28);
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}

	Button::~Button()
	{

	}

	void Button::setPos(sf::Vector2f vector)
	{
		back.setPosition(vector);
		background.setPosition(sf::Vector2f(back.getPosition().x - 2.f, back.getPosition().y - 2.f));
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}

	void Button::setSize(sf::Vector2f vector)
	{
		back.setSize(vector);
		background.setSize(sf::Vector2f(back.getSize().x + 4.f, back.getSize().y + 4.f));
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}

	void Button::setString(const char* input)
	{
		text.setString(input);
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}

	void Button::setCharSize(int size)
	{
		text.setCharacterSize(size);
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}

	void Button::setClickSound(sf::Sound *clickSound)
	{
		this->clickSound = clickSound;
	}

	sf::Vector2f Button::getSize()
	{
		return back.getSize();
	}

	sf::Vector2f Button::getPosition()
	{
		return back.getPosition();
	}

	std::string Button::getString()
	{
		return text.getString();
	}

	bool Button::processInput(sf::Vector2f worldPos, const sf::Event event)
	{
		if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
		{
			back.setFillColor(sf::Color::White);
			text.setFillColor(sf::Color::Black);
			return (back.getGlobalBounds().contains(worldPos) && wasLClicked);
		}
		else if (event.type == sf::Event::MouseMoved)
		{
			if (back.getGlobalBounds().contains(worldPos) && back.getFillColor() != sf::Color(44, 44, 44, 255))
				back.setFillColor(sf::Color(235, 165, 0, 255));
			else if (back.getGlobalBounds().contains(worldPos) && back.getFillColor() == sf::Color(44, 44, 44, 255))
				return false;
			else
			{
				back.setFillColor(sf::Color::White);
				text.setFillColor(sf::Color::Black);
			}
		}
		else if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left)
		{
			if (back.getGlobalBounds().contains(worldPos))
			{
				back.setFillColor(sf::Color(44, 44, 44, 255));
				text.setFillColor(sf::Color::White);
				wasLClicked = true;
				clickSound->play();
			}
			else
				wasLClicked = false;
		}
		return false;
	}

	void Button::isHovered(sf::Vector2f worldPos)
	{
		if (back.getGlobalBounds().contains(worldPos) && back.getFillColor() != sf::Color(44, 44, 44, 255))
			back.setFillColor(sf::Color(235, 165, 0, 255));
		else if (back.getGlobalBounds().contains(worldPos) && back.getFillColor() == sf::Color(44, 44, 44, 255))
			return;
		else
		{
			back.setFillColor(sf::Color::White);
			text.setFillColor(sf::Color::Black);
		}
	}

	void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(background, states);
		target.draw(back, states);
		target.draw(text, states);
	}
}