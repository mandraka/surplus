#include "DropDown.h"
#include <iostream>
#include <SFML/Audio.hpp>

namespace gui
{
	//
	DropDown::DropDown(int rows, std::vector<std::string> inside, sf::Vector2f pos, sf::Sound *listSound)
	{
		if (!font.loadFromFile("fonts/OpenSans-Regular.ttf"))
			std::cout << "Can't load font";
		this->listSound = listSound;
		content = new sf::Text[rows]();
		contentBack = new sf::RectangleShape[rows]();
		isExpanded = false;
		selectedIndex = -1;
		rowsCount = rows;

		mainBack.setPosition(pos);
		mainBack.setSize(sf::Vector2f(180, 30));
		mainText.setString("");
		mainText.setFont(font);
		mainText.setCharacterSize(20);
		mainText.setPosition(mainBack.getPosition().x + mainBack.getSize().x / 2 - mainText.getLocalBounds().width / 2,
			mainBack.getPosition().y + mainBack.getSize().y / 2 - mainText.getCharacterSize() / 2 - 5);

		for (int i = 0; i < rowsCount; i++)
		{
			contentBack[i].setFillColor(sf::Color::Black);
			contentBack[i].setPosition(sf::Vector2f(pos.x, pos.y + (i + 1) * 30));
			contentBack[i].setSize(sf::Vector2f(180, 30));
			content[i].setFillColor(sf::Color::White);
			content[i].setFont(font);
			content[i].setCharacterSize(20);
			content[i].setString(inside[i]);
			content[i].setPosition(contentBack[i].getPosition().x + contentBack[i].getSize().x / 2 - content[i].getLocalBounds().width / 2,
				contentBack[i].getPosition().y + contentBack[i].getSize().y / 2 - content[i].getCharacterSize() / 2);
		}
	}

	DropDown::DropDown(int rows, std::vector<std::string> inside, sf::Vector2f pos, sf::Vector2f size, sf::Sound *listSound) : DropDown(rows, inside, pos, listSound)
	{
		mainBack.setSize(size);
		mainText.setCharacterSize(20);
		mainText.setPosition(mainBack.getPosition().x + mainBack.getSize().x / 2 - mainText.getLocalBounds().width / 2,
			mainBack.getPosition().y + mainBack.getSize().y / 2 - mainText.getCharacterSize() / 2 - 5);
		for (int i = 0; i < rowsCount; i++)
		{
			contentBack[i].setPosition(sf::Vector2f(pos.x, pos.y + (i + 1) * size.y));
			contentBack[i].setSize(size);
			content[i].setPosition(contentBack[i].getPosition());
			content[i].setCharacterSize(20);
			content[i].setPosition(contentBack[i].getPosition().x + contentBack[i].getSize().x / 2 - content[i].getLocalBounds().width / 2,
				contentBack[i].getPosition().y + contentBack[i].getSize().y / 2 - content[i].getCharacterSize() / 2);
		}
	}

	DropDown::~DropDown()
	{
		delete[] content;
		delete[] contentBack;
	}

	void DropDown::reset()
	{
		selectedIndex = -1;
		isExpanded = false;
		mainText.setString("");
	}

	int DropDown::getSelectedIndex()
	{
		return selectedIndex;
	}

	void DropDown::processInput(sf::Vector2f worldPos, sf::Event *event)
	{
		if (!isExpanded)
		{
			if (event->type == sf::Event::MouseMoved)
			{
				if (mainBack.getGlobalBounds().contains(worldPos) && mainBack.getFillColor() != sf::Color(44, 44, 44, 255))
					mainBack.setFillColor(sf::Color(235, 165, 0, 255));
				else if (mainBack.getGlobalBounds().contains(worldPos) && mainBack.getFillColor() == sf::Color(44, 44, 44, 255))
				{
				}
				else
				{
					mainBack.setFillColor(sf::Color::White);
					mainText.setFillColor(sf::Color::Black);
				}
			}
			else if (event->type == sf::Event::MouseButtonPressed && event->mouseButton.button == sf::Mouse::Left)
			{
				if (mainBack.getGlobalBounds().contains(worldPos))
				{
					mainBack.setFillColor(sf::Color(44, 44, 44, 255));
					mainText.setFillColor(sf::Color::White);
					isExpanded = !isExpanded;
					//listSound->play();
				}
			}
		}
		else if (isExpanded)
		{
			if (event->type == sf::Event::MouseMoved)
			{
				for (int i = 0; i < rowsCount; i++)
				{
					if (contentBack[i].getGlobalBounds().contains(worldPos))
						contentBack[i].setFillColor(sf::Color(235, 165, 0, 255));
					else
						contentBack[i].setFillColor(sf::Color::Black);
				}	
			}
			else if (event->type == sf::Event::MouseButtonPressed && event->mouseButton.button == sf::Mouse::Left)
			{
				if (mainBack.getGlobalBounds().contains(worldPos))
				{
					mainBack.setFillColor(sf::Color(44, 44, 44, 255));
					mainText.setFillColor(sf::Color::White);
					isExpanded = !isExpanded;
					//listSound->play();
					return;
				}
				else 
				{
					for (int i = 0; i < rowsCount; i++)
					{
						if (contentBack[i].getGlobalBounds().contains(worldPos))
						{
							selectedIndex = i;
							mainText.setString(content[i].getString());
							mainText.setPosition(mainBack.getPosition().x + mainBack.getSize().x / 2 - mainText.getLocalBounds().width / 2,
								mainBack.getPosition().y + mainBack.getSize().y / 2 - mainText.getCharacterSize() / 2 - 5);
							listSound->play();
							isExpanded = !isExpanded;
							sf::Event e = sf::Event();
							*event = e;
							return;
						}
					}
				}
				isExpanded = false;
				mainBack.setFillColor(sf::Color::White);
				mainText.setFillColor(sf::Color::Black);
			}
		}
	}

	void DropDown::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(mainBack, states);
		target.draw(mainText, states);
		if (isExpanded)
		{
			for (int i = 0; i < rowsCount; i++)
			{
				target.draw(contentBack[i], states);
				target.draw(content[i], states);
			}
		}

	}
}
