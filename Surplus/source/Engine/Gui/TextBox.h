#pragma once
#include <SFML/Graphics.hpp>
#include <atomic>

namespace std
{
	class thread;
}

namespace gui
{
	class TextBox : public sf::Drawable
	{
	private:
		sf::Font font;
		bool showCursor;
	//	unsigned cursorIndex;
		sf::RectangleShape cursor;
		sf::RectangleShape back;
		sf::RectangleShape background;
		sf::Text text;
		sf::String input;
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		bool numbersOnly;
		std::thread *t;
		std::atomic<bool> cancellationBool;
	public:
		int maxValue = -2147483647;
		sf::Clock clock;
		gui::TextBox *other;
		TextBox();
		TextBox(sf::Vector2f pos, sf::Vector2f size);
		~TextBox();
		void reset();
		void setPos(sf::Vector2f);
		void setSize(sf::Vector2f);
		void setString(const char*);
		void setCharSize(int);
		sf::Vector2f getSize();
		sf::Vector2f getPosition();
		void processInput(sf::Vector2f, const sf::Event);
		void processText(const sf::Event);
		sf::String getInput();
		int getIntInput();
		void switchSelection();
		void switchSelection(bool);
		void setNumbersOnly(bool);
		void clockSync();
	};
}

