#include "TextBox.h"
#include <iostream>
#include <thread>


namespace gui
{
	TextBox::TextBox()
	{
		if (!font.loadFromFile("fonts/OpenSans-Regular.ttf"))
			std::cout << "Can't load font";
		numbersOnly = false;

		back.setSize(sf::Vector2f(200.f, 60.f));
		back.setPosition(sf::Vector2f(100.f, 100.f));
		background.setFillColor(sf::Color::Black);
		background.setSize(sf::Vector2f(back.getSize().x + 4.f, back.getSize().y + 4.f));
		background.setPosition(sf::Vector2f(back.getPosition().x - 2.f, back.getPosition().y - 2.f));
		text.setString("");
		text.setFont(font);
		text.setCharacterSize(24);
		text.setFillColor(sf::Color::Black);
		text.setPosition(back.getPosition().x + 10,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);

		showCursor = false;
		//cursorIndex = 0;
		cursor.setSize(sf::Vector2f(1.2f, 40.f));
		cursor.setFillColor(sf::Color::Black);
		cursor.setPosition(110.f, 110.f);

		clock.restart();
		//clock restarting thread
		cancellationBool = false;
		t = new std::thread(&TextBox::clockSync, this);
	}

	TextBox::TextBox(sf::Vector2f pos, sf::Vector2f size) : TextBox()
	{
		back.setSize(size);
		back.setPosition(pos);
		background.setSize(sf::Vector2f(back.getSize().x + 4.f, back.getSize().y + 4.f));
		background.setPosition(sf::Vector2f(back.getPosition().x - 2.f, back.getPosition().y - 2.f));
		text.setCharacterSize(18 + unsigned(size.x) / 28);
		text.setPosition(back.getPosition().x + 10,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);

		cursor.setSize(sf::Vector2f(1.2f, (size.y * 2) / 3));
		cursor.setPosition(sf::Vector2f(back.getPosition().x + 10 + text.getString().getSize() * 14.2f,
			back.getPosition().y + back.getSize().y / 6));
	}

	TextBox::~TextBox()
	{
		cancellationBool = true;
		t->join();
		delete t;
	}

	void TextBox::reset()
	{
		text.setString("");
		input = "";
		showCursor = false;
		cursor.setPosition(sf::Vector2f(back.getPosition().x + 10 + text.getString().getSize() * 14.2f,
			back.getPosition().y + back.getSize().y / 6));
	}

	void TextBox::setPos(sf::Vector2f vector)
	{
		back.setPosition(vector);
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}

	void TextBox::setSize(sf::Vector2f vector)
	{
		back.setSize(vector);
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}

	void TextBox::setString(const char* input)
	{
		text.setString(input);
		this->input = input;
		cursor.setPosition(sf::Vector2f(back.getPosition().x + 10 + text.getString().getSize() * 14.2f,
			back.getPosition().y + back.getSize().y / 6));
	}

	void TextBox::setCharSize(int size)
	{
		text.setCharacterSize(size);
		text.setPosition(back.getPosition().x + back.getSize().x / 2 - text.getLocalBounds().width / 2,
			back.getPosition().y + back.getSize().y / 2 - text.getCharacterSize() / 2 - 5);
	}

	sf::Vector2f TextBox::getSize()
	{
		return back.getSize();
	}

	sf::Vector2f TextBox::getPosition()
	{
		return back.getPosition();
	}

	void TextBox::processInput(sf::Vector2f worldPos, const sf::Event event)
	{

		if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left)
		{
			if (back.getGlobalBounds().contains(worldPos))
			{
				clock.restart();
				switchSelection(true);
				if (other != nullptr)
				{
					other->switchSelection(false);
				}
			}
		}
		else if (showCursor)
		{
			processText(event);
		}
	}

	void TextBox::processText(const sf::Event event)
	{
		if (event.type == sf::Event::TextEntered)
		{
			//remove
			if (event.text.unicode == '\b' && showCursor == true && input.getSize() > 0)
			{
				input.erase(input.getSize() - 1, 1);
				text.setString(input);
				cursor.setPosition(sf::Vector2f(back.getPosition().x + 15 + text.getGlobalBounds().width,
					back.getPosition().y + back.getSize().y / 6));
			}
			//add
			if (event.text.unicode > 47 && event.text.unicode < 58)
			{
				if (showCursor == true && text.getGlobalBounds().width < back.getSize().x - 20)
				{
					input += static_cast<char>(event.text.unicode);
					text.setString(input);
					cursor.setPosition(sf::Vector2f(back.getPosition().x + 15 + text.getGlobalBounds().width,
						back.getPosition().y + back.getSize().y / 6));
					if (maxValue > -2147483647 && numbersOnly)
					{
						std::string s = input.toAnsiString();
						if (std::stoi(s) > maxValue)
							input = std::to_string(maxValue);
						text.setString(input);
						cursor.setPosition(sf::Vector2f(back.getPosition().x + 10 + text.getString().getSize() * 14.2f,
							back.getPosition().y + back.getSize().y / 6));
					}
				}
			}
			if (!numbersOnly)
			{
				if (event.text.unicode > 57 && event.text.unicode < 123)
				{
					if (showCursor == true && text.getGlobalBounds().width < back.getSize().x - 20)
					{
						input += static_cast<char>(event.text.unicode);
						text.setString(input);
						cursor.setPosition(sf::Vector2f(back.getPosition().x + 15 + text.getGlobalBounds().width,
							back.getPosition().y + back.getSize().y / 6));
					}
				}
			}
		}
	}

	sf::String TextBox::getInput()
	{
		return this->input;
	}
	int TextBox::getIntInput()
	{
		if (!numbersOnly)
			return -1;
		std::string s = input;
		return std::stoi(s, nullptr, 10);
	}

	void TextBox::switchSelection()
	{
		this->showCursor = !showCursor;
	}
	void TextBox::switchSelection(bool to)
	{
		this->showCursor = to;
	}
	void TextBox::setNumbersOnly(bool nO)
	{
		this->numbersOnly = nO;
	}
	void TextBox::clockSync()
	{
		while (!cancellationBool)
		{
			if (clock.getElapsedTime().asMilliseconds() > 1000)
				clock.restart();
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}

	}

	void TextBox::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(background, states);
		target.draw(back, states);
		target.draw(text, states);
		if (showCursor == true && clock.getElapsedTime().asMilliseconds() < 650)
			target.draw(cursor, states);
	}
}