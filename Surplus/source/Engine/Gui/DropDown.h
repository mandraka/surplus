#pragma once
#include <SFML/Graphics.hpp>

namespace sf
{
	class Sound;
}

namespace gui
{
	class DropDown : public sf::Drawable
	{
	private:
		sf::Sound *listSound;
		sf::Font font;
		sf::RectangleShape mainBack;
		sf::RectangleShape background;
		sf::Text mainText;
		sf::RectangleShape *contentBack;
		sf::Text *content;
		bool isExpanded;
		int selectedIndex;
		int rowsCount;
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	public:
		DropDown(int, std::vector<std::string>, sf::Vector2f, sf::Sound *);
		DropDown(int, std::vector<std::string>, sf::Vector2f, sf::Vector2f, sf::Sound *);
		~DropDown();
		void reset();
		int getSelectedIndex();
		void processInput(sf::Vector2f, sf::Event *);
	};
}

