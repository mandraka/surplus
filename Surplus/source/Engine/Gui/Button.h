#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

namespace gui
{
	class Button : public sf::Drawable
	{
	private:
		bool wasLClicked;
		sf::Font font;
		sf::RectangleShape back;
		sf::RectangleShape background;
		sf::Sound* clickSound;
		sf::Text text;
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	public:
		Button();
		Button(const char*);
		Button(const char*, sf::Vector2f);
		Button(const char*, sf::Vector2f, sf::Vector2f, sf::Sound *);
		~Button();
		void setPos(sf::Vector2f);
		void setSize(sf::Vector2f);
		void setString(const char*);
		void setCharSize(int);
		void setClickSound(sf::Sound *);
		sf::Vector2f getSize();
		sf::Vector2f getPosition();
		std::string getString();
		bool processInput(sf::Vector2f, const sf::Event);
		void isHovered(sf::Vector2f);
	};
}

