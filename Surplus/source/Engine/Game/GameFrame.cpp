#include <iostream>
#include <SFML/Audio.hpp>
#include "GameFrame.h"
#include "../Gui/Button.h"
#include "../Gui/TextBox.h"
#include "../Gui/ProgressBar.h"
#include "../Map.h"
#include "../../Fields/Hexagon.h"

GameFrame::GameFrame(sf::RenderWindow* window, stance* mainStance)
{
	this->window = window;
	this->mainStance = mainStance;
	globalScale = new sf::Vector2f(0.2f, 0.2f);
	//initialize scrolling
	scrollUp = false; scrollDown = false; scrollRight = false; scrollLeft = false;
	scrollSpeed = 100;
	menuOn = false;
	entitiesOn = false;
	selected = false;
	initializeMenu();
	initializeUi();
	initializeEntities();
	loadStatics();
	eMode = eNull;
}


GameFrame::~GameFrame()
{
	delete textBox;
	delete saveToFileButton;
	delete returnButton;
	delete continueButton;
	delete saveButton;
	delete exitButton;
	delete infrastructureButton;
	delete townButton;
	delete supplyDepotButton;
	delete factoryButton;
	delete unitsButton;
	delete cancelButton;
	delete upgradeButton;
	delete milAddButton;
	delete logAddButton;
	delete unitMove;
	delete[] factBack;
}

void GameFrame::loadStatics()
{
	if (!font.loadFromFile("fonts/OpenSans-Regular.ttf"))
		std::cout << "Can't load font";
	if (!baseHex.loadFromFile("textures/hexagon-base.png"))
		std::cout << "Main hexagon png not found";
	baseSprite.setTexture(baseHex);
	if (!nationHex.loadFromFile("textures/hexagon-nationSelect.png"))
		std::cout << "Nation outline png not found";
	nationSprite.setTexture(nationHex);
	//entities
	if (!townTexture.loadFromFile("textures/town.png"))
		std::cout << "Town texture not found";
	townSprite.setTexture(townTexture);
	if (!warehouseTexture.loadFromFile("textures/warehouse.png"))
		std::cout << "Warehouse texture not found";
	warehouseSprite.setTexture(warehouseTexture);
	if (!factoryTexture.loadFromFile("textures/factory.png"))
		std::cout << "Factory texture not found";
	factorySprite.setTexture(factoryTexture);
	if (!infra1Texture.loadFromFile("textures/infra1.png"))
		std::cout << "Roads texture not found";
	infra1Sprite.setTexture(infra1Texture);
	if (!infra2Texture.loadFromFile("textures/infra2.png"))
		std::cout << "Railroads texture not found";
	infra2Sprite.setTexture(infra2Texture);
	//units
	if (!milUnitTexture.loadFromFile("textures/milUnit.png"))
		std::cout << "Infantry divison texture not found";
	milUnitSprite.setTexture(milUnitTexture);
	if (!logUnitTexture.loadFromFile("textures/logUnit.png"))
		std::cout << "Logistic divison texture not found";
	logUnitSprite.setTexture(logUnitTexture);
	//resources
	if (!ironTexture.loadFromFile("textures/iron.png"))
		std::cout << "Iron texture not found";
	ironSprite.setTexture(ironTexture);
	if (!foodTexture.loadFromFile("textures/food.png"))
		std::cout << "Food texture not found";
	foodSprite.setTexture(foodTexture);
	if (!coalTexture.loadFromFile("textures/coal.png"))
		std::cout << "Coal texture not found";
	coalSprite.setTexture(coalTexture);
	if (!steelTexture.loadFromFile("textures/steel.png"))
		std::cout << "Steel texture not found";
	steelSprite.setTexture(steelTexture);
	if (!woodTexture.loadFromFile("textures/wood.png"))
		std::cout << "Wood texture not found";
	woodSprite.setTexture(woodTexture);
	if (!supplyTexture.loadFromFile("textures/supply.png"))
		std::cout << "Supply texture not found";
	supplySprite.setTexture(supplyTexture);
	updateScales();
	//audio
	if (!clickBuffer.loadFromFile("sounds/click.wav"))
		std::cout << "Can't load click sound";
	clickSound.setBuffer(clickBuffer);
	//	if (!listBuffer.loadFromFile("sounds/list.wav"))
	if (!listBuffer.loadFromFile("sounds/click.wav"))
		std::cout << "Can't load list sound";
	listSound.setBuffer(listBuffer);
}

void GameFrame::updateScales()
{
	baseSprite.setScale(*globalScale);
	hexOffset = baseSprite.getGlobalBounds().height / 2;
	nationSprite.setScale(*globalScale);
	townSprite.setScale(*globalScale);
	warehouseSprite.setScale(*globalScale);
	factorySprite.setScale(*globalScale);
	infra1Sprite.setScale(*globalScale);
	infra2Sprite.setScale(*globalScale);
	milUnitSprite.setScale(*globalScale);
	logUnitSprite.setScale(*globalScale);
	ironSprite.setScale(*globalScale);
	foodSprite.setScale(*globalScale);
	coalSprite.setScale(*globalScale);
	steelSprite.setScale(*globalScale);
	woodSprite.setScale(*globalScale);
	supplySprite.setScale(*globalScale);
	unitMove->setScale(*globalScale);
}

void GameFrame::initializeMenu()
{
	menuShade.setSize(sf::Vector2f((float)(window->getSize().x), (float)(window->getSize().y)));
	menuShade.setFillColor(sf::Color(0, 0, 0, 100));
	menuBackground.setSize(sf::Vector2f(600.f, window->getSize().y / 1.85f));
	menuBackground.setFillColor(sf::Color(0, 0, 0, 215));
	menuBackground.setPosition(sf::Vector2f(window->getSize().x / 2.f - 300.f, window->getSize().y / 7.f - 100));
	continueButton = new gui::Button("Continue Game", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 7.f),
		sf::Vector2f(300.f, 80.f), &clickSound);
	saveButton = new gui::Button("Save Game", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 7.f * 2.f),
		sf::Vector2f(300.f, 80.f), &clickSound);
	exitButton = new gui::Button("Exit to Main Menu", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 7.f * 3.f),
		sf::Vector2f(300.f, 80.f), &clickSound);

	saveBackground.setSize(sf::Vector2f(600.f, window->getSize().y / 1.85f));
	saveBackground.setFillColor(sf::Color(0, 0, 0, 255));
	saveBackground.setPosition(sf::Vector2f(window->getSize().x / 2.f - 300.f, window->getSize().y / 7.f - 100));
	textBox = new gui::TextBox(sf::Vector2f(window->getSize().x / 2.f - 100.f, window->getSize().y / 4.35f),
		sf::Vector2f(200.f, 60.f));
	saveToFileButton = new gui::Button("Save Game", sf::Vector2f(window->getSize().x / 2.f - 100.f, window->getSize().y*1.4f / 4.35f),
		sf::Vector2f(200.f, 60.f), &clickSound);
	returnButton = new gui::Button("Return", sf::Vector2f(window->getSize().x / 2.f - 100.f, window->getSize().y*1.4f / 4.35f + saveToFileButton->getSize().y + 10),
		sf::Vector2f(200.f, 60.f), &clickSound);
}

void GameFrame::initializeUi()
{
	uiBackground.setSize(sf::Vector2f((float)(window->getSize().x / 6 + 1), (float)(window->getSize().y)));
	uiBackground.setFillColor(sf::Color(0, 0, 0, 211));
	uiBackground.setPosition(sf::Vector2f((5 * window->getSize().x) / 6.f, 0));
	selectedCoordsLabel.setFont(font);
	selectedCoordsLabel.setCharacterSize(22);
	selectedCoordsLabel.setFillColor(sf::Color::White);
	selectedCoordsLabel.setPosition((5.f * window->getSize().x) / 6.f + 15, 5);
	terrainLabel.setFont(font);
	terrainLabel.setCharacterSize(22);
	terrainLabel.setFillColor(sf::Color::White);
	terrainLabel.setPosition((5.f * window->getSize().x) / 6.f + 15, selectedCoordsLabel.getPosition().y + selectedCoordsLabel.getCharacterSize() * 3);
	nationalityLabel.setFont(font);
	nationalityLabel.setCharacterSize(22);
	nationalityLabel.setFillColor(sf::Color::White);
	nationalityLabel.setPosition((5.f * window->getSize().x) / 6.f + 15, terrainLabel.getPosition().y + 160);
	resourceLabel.setFont(font);
	resourceLabel.setCharacterSize(22);
	resourceLabel.setFillColor(sf::Color::White);
	resourceLabel.setPosition((5.f * window->getSize().x) / 6.f + 15, nationalityLabel.getPosition().y + 120);
	entitiesLabel.setFont(font);
	entitiesLabel.setCharacterSize(18);
	entitiesLabel.setFillColor(sf::Color::White);
	entitiesLabel.setPosition((5.f * window->getSize().x) / 6.f + 15, resourceLabel.getPosition().y + 120);
	//entities
	infrastructureButton = new gui::Button("Infrastructure", sf::Vector2f(uiBackground.getPosition().x + 5,
		entitiesLabel.getPosition().y + entitiesLabel.getCharacterSize() * 8.f), sf::Vector2f(120.f, 50.f), &clickSound);
	infrastructureButton->setCharSize(16);
	townButton = new gui::Button("City", sf::Vector2f(uiBackground.getPosition().x + 10 + infrastructureButton->getSize().x,
		infrastructureButton->getPosition().y), sf::Vector2f(60.f, 50.f), &clickSound);
	supplyDepotButton = new gui::Button("Supply Depot", sf::Vector2f(uiBackground.getPosition().x + 15 +
		townButton->getSize().x + infrastructureButton->getSize().x,
		infrastructureButton->getPosition().y), sf::Vector2f(120.f, 50.f), &clickSound);
	supplyDepotButton->setCharSize(16);
	factoryButton = new gui::Button("Factory", sf::Vector2f(uiBackground.getPosition().x + 5,
		entitiesLabel.getPosition().y + entitiesLabel.getCharacterSize() * 8.f + infrastructureButton->getSize().y + 10),
		sf::Vector2f(120.f, 60.f), &clickSound);
	unitsButton = new gui::Button("Units", sf::Vector2f(supplyDepotButton->getPosition().x, factoryButton->getPosition().y),
		sf::Vector2f(120.f, 60.f), &clickSound);

	unitMove = new gui::ProgressBar();
	unitMove->setSize(sf::Vector2f(200, 40));
}

void GameFrame::initializeEntities()
{
	entitiesShade.setSize(sf::Vector2f((float)(window->getSize().x), (float)(window->getSize().y)));
	entitiesShade.setFillColor(sf::Color(0, 0, 0, 100));
	entitiesBackground.setSize(sf::Vector2f(600.f, window->getSize().y / 1.85f));
	entitiesBackground.setFillColor(sf::Color(0, 0, 0, 215));
	entitiesBackground.setPosition(sf::Vector2f(window->getSize().x / 2.f - 300.f, window->getSize().y / 7.f - 100));
	cancelButton = new gui::Button("Cancel", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 7.f * 3.f),
		sf::Vector2f(260.f, 60.f), &clickSound);
	upgradeButton = new gui::Button("Upgrade", sf::Vector2f(window->getSize().x / 2.f - 200.f, window->getSize().y / 7.f * 2.f),
		sf::Vector2f(200.f, 60.f), &clickSound);
	goodsLabel.setFont(font);
	goodsLabel.setCharacterSize(18);
	goodsLabel.setFillColor(sf::Color::White);
	goodsLabel.setPosition(sf::Vector2f(window->getSize().x / 2.f - 260.f, window->getSize().y / 7.f - 80));
	//units
	milAddButton = new gui::Button("Add military unit", sf::Vector2f(window->getSize().x / 2.f - 200.f, window->getSize().y / 7.f * 2.f - 100),
		sf::Vector2f(200.f, 60.f), &clickSound);
	logAddButton = new gui::Button("Add logistic unit", sf::Vector2f(window->getSize().x / 2.f - 200.f, window->getSize().y / 7.f * 2.f),
		sf::Vector2f(200.f, 60.f), &clickSound);
	milLabel.setFont(font);
	milLabel.setCharacterSize(18);
	milLabel.setFillColor(sf::Color::White);
	milLabel.setPosition(sf::Vector2f(window->getSize().x / 2.f + 20, window->getSize().y / 7.f * 2.f - 100));
	logLabel.setFont(font);
	logLabel.setCharacterSize(18);
	logLabel.setFillColor(sf::Color::White);
	logLabel.setPosition(sf::Vector2f(window->getSize().x / 2.f + 20, window->getSize().y / 7.f * 2.f));
	//factory
	factBack = new gui::Button[3]();
	for (int i = 0; i < 3; i++)
	{
		factBack[i].setSize(sf::Vector2f(50, 50));
		factBack[i].setClickSound(&clickSound);
	}
	//	factBack = new gui::Button(sf::Vector2f(50, 50));

}

void GameFrame::processGameInput(sf::Event event)
{
	//map scrolling
	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Up)
			scrollUp = true;
		else if (event.key.code == sf::Keyboard::Down)
			scrollDown = true;
		else if (event.key.code == sf::Keyboard::Left)
			scrollLeft = true;
		else if (event.key.code == sf::Keyboard::Right)
			scrollRight = true;
	}
	else if (event.type == sf::Event::KeyReleased)
	{
		if (event.key.code == sf::Keyboard::Up)
			scrollUp = false;
		else if (event.key.code == sf::Keyboard::Down)
			scrollDown = false;
		else if (event.key.code == sf::Keyboard::Left)
			scrollLeft = false;
		else if (event.key.code == sf::Keyboard::Right)
			scrollRight = false;
	}

	//map zoom
	else if (event.type == sf::Event::MouseWheelScrolled)
	{
		sf::Vector2f v(event.mouseWheelScroll.delta / 40, event.mouseWheelScroll.delta / 40);
		float frst = float(sf::Mouse::getPosition(*window).x);
		float scnd = float(window->getSize().x / 2);
		float trd = float(sf::Mouse::getPosition(*window).y);
		float frth = float(window->getSize().y / 2);
		float zmiennaX = (frst - scnd);
		float zmiennaY = (trd - frth);
		if (scrollOffset.x < int(window->getSize().x / 2) && sf::Mouse::getPosition(*window).x < int(window->getSize().x / 2))
			zmiennaX = 0.4f*zmiennaX;
		if (scrollOffset.y < int(window->getSize().y / 2) && sf::Mouse::getPosition(*window).y < int(window->getSize().y / 2))
			zmiennaY = 0.4f*zmiennaY;
		if (event.mouseWheelScroll.delta > 0 && baseSprite.getScale().x < 1)
		{
			*globalScale += v;
			if (scrollOffset.x < maxScrollX - ((scrollOffset.x * 0.24f) + window->getSize().x * 0.095f + zmiennaX * 0.3f) / (baseSprite.getScale().x * 9.2f))
				scrollOffset.x += ((scrollOffset.x * 0.24f) + (window->getSize().x * 0.095f) + zmiennaX * 0.3f) / (baseSprite.getScale().x * 9.2f);
			if (scrollOffset.y < maxScrollY - ((scrollOffset.y * 0.24f) + window->getSize().y * 0.115f + zmiennaX * 0.3f) / (baseSprite.getScale().y * 9.2f))
				scrollOffset.y += ((scrollOffset.y * 0.24f) + (window->getSize().y * 0.115f) + zmiennaY * 0.3f) / (baseSprite.getScale().y * 9.2f);
			updateScales();
			maxScrollY = int((baseSprite.getGlobalBounds().height * map->getSize().y)
				+ hexOffset - window->getSize().y - 10);
			maxScrollX = int((baseSprite.getGlobalBounds().width*0.7501f) * map->getSize().x - window->getSize().x + 10);
			maxScrollX += window->getSize().x / 6;
		}
		else if (event.mouseWheelScroll.delta < 0 && baseSprite.getScale().x>0.05f)
		{
			*globalScale += v;
			if (scrollOffset.x > ((scrollOffset.x * 0.14f) + (window->getSize().x * 0.08f) + zmiennaX * 0.3f) / (baseSprite.getScale().x * 5.8f))
				scrollOffset.x -= ((scrollOffset.x * 0.14f) + (window->getSize().x * 0.08f) + zmiennaX * 0.3f) / (baseSprite.getScale().x * 5.8f);
			if (scrollOffset.y > ((scrollOffset.x * 0.14f) + (window->getSize().y * 0.08f) + zmiennaY * 0.3f) / (baseSprite.getScale().x * 5.8f))
				scrollOffset.y -= ((scrollOffset.y * 0.14f) + (window->getSize().y * 0.08f) + zmiennaY * 0.3f) / (baseSprite.getScale().y * 5.8f);
			updateScales();
			maxScrollY = int((baseSprite.getGlobalBounds().height * map->getSize().y)
				+ hexOffset - window->getSize().y - 10);
			maxScrollX = int((baseSprite.getGlobalBounds().width*0.7501f) * map->getSize().x - window->getSize().x + 10);
			maxScrollX += window->getSize().x / 6;
		}
		scrollSpeed = 20.f / globalScale->x;
	}

	//mouse click	
	if (event.type == sf::Event::MouseButtonReleased)
	{
		sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
		sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			if (worldPos.x < uiBackground.getPosition().x)
			{

				//basic square selection
				int selectedHexX = int((worldPos.x + scrollOffset.x) / (baseSprite.getGlobalBounds().width*0.75f));
				int selectedHexY;
				if ((int)((worldPos.x + scrollOffset.x) / (baseSprite.getGlobalBounds().width*0.75f)) % 2 == 0)
					selectedHexY = int((worldPos.y + scrollOffset.y) / baseSprite.getGlobalBounds().height);
				else
					selectedHexY = (int(((worldPos.y + scrollOffset.y) + baseSprite.getGlobalBounds().height / 2) / baseSprite.getGlobalBounds().height)) - 1;

				//hex diagonals magics
				bool parityHex = true;
				int localHexXPos = int((worldPos.x + scrollOffset.x) - (selectedHexX * (baseSprite.getGlobalBounds().width*0.75f)));
				int localHexYPos = int((worldPos.y + scrollOffset.y) - (selectedHexY * baseSprite.getGlobalBounds().height));
				if ((int)((worldPos.x + scrollOffset.x) / (baseSprite.getGlobalBounds().width*0.75f)) % 2 == 1)
				{
					localHexYPos -= int(baseSprite.getGlobalBounds().height / 2);
					parityHex = false;
				}
				if (localHexXPos + localHexYPos < 170 * baseSprite.getScale().x)
				{
					selectedHexX--;
					if (parityHex)
						selectedHexY--;
				}
				else if (localHexYPos - localHexXPos > 300 * baseSprite.getScale().x)
				{
					selectedHexX--;
					selectedHexY++;
					if (parityHex)
						selectedHexY--;
				}
				//unselecting
				if (selectedHexY >= map->getSize().y || selectedHexY < 0)
				{
					selected = false;
					if (map->lastSelected.y != -1)
						map->hexagons[map->lastSelected.y][map->lastSelected.x].switchSelection();
					map->lastSelected.y = -1;
					return;
				}
				if (selectedHexX >= map->getSize().x || selectedHexX < 0)
				{
					selected = false;
					if (map->lastSelected.y != -1)
						map->hexagons[map->lastSelected.y][map->lastSelected.x].switchSelection();
					map->lastSelected.y = -1;
					return;
				}

				selected = map->hexagons[selectedHexY][selectedHexX].switchSelection();
				if (map->lastSelected.y != -1 && selected)
					map->hexagons[map->lastSelected.y][map->lastSelected.x].switchSelection();
				if (!selected)
					map->lastSelected.y = -1;
				if (selected)
				{
					selectedV = sf::Vector2i(selectedHexX, selectedHexY);
					map->lastSelected.x = selectedV.x;
					map->lastSelected.y = selectedV.y;
				}
			}
		}
	}
}

void GameFrame::scrollHandle()
{
	if (scrollUp)
	{
		scrollOffset.y -= scrollSpeed * baseSprite.getScale().y;
		if (scrollOffset.y < -15)
			scrollOffset.y += scrollSpeed * baseSprite.getScale().y;
	}
	if (scrollDown)
	{
		scrollOffset.y += scrollSpeed * baseSprite.getScale().y;
		if (scrollOffset.y >= maxScrollY + scrollSpeed * baseSprite.getScale().y)
			scrollOffset.y -= scrollSpeed * baseSprite.getScale().y;
	}
	if (scrollLeft)
	{
		scrollOffset.x -= scrollSpeed * baseSprite.getScale().x;
		if (scrollOffset.x < -15)
			scrollOffset.x += scrollSpeed * baseSprite.getScale().x;
	}
	if (scrollRight)
	{
		scrollOffset.x += scrollSpeed * baseSprite.getScale().x;
		if (scrollOffset.x >= maxScrollX + scrollSpeed * baseSprite.getScale().y)
			scrollOffset.x -= scrollSpeed * baseSprite.getScale().x;
	}
}

void GameFrame::drawingMainScreen()
{
	window->clear(sf::Color(55, 66, 66, 255));

	int yViewPos = int(scrollOffset.y / baseSprite.getGlobalBounds().height);
	if (yViewPos > 0) yViewPos--;
	int yToPos = int(yViewPos + 3 + (window->getSize().y / baseSprite.getGlobalBounds().height));
	while (yToPos > map->getSize().y) yToPos--;

	int xViewPos = int(scrollOffset.x / (baseSprite.getGlobalBounds().width*0.75f));
	if (xViewPos > 0) xViewPos--;
	int xToPos = int(xViewPos + 3 + (window->getSize().x / (baseSprite.getGlobalBounds().width*0.75f)));
	while (xToPos > map->getSize().x) xToPos--;

	for (int i = yViewPos; i < yToPos; i++) //iterate rows
	{
		for (int j = xViewPos; j < xToPos; j++) //iterate columns
		{
			if (map->hexagons[i][j].visible)
			{
				baseSprite.setPosition(sf::Vector2f(baseSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
					baseSprite.getGlobalBounds().height * i - scrollOffset.y));
				if (j % 2 == 1) baseSprite.move(sf::Vector2f(0, hexOffset));
				switch (map->hexagons[i][j].getTerrain())
				{
				case plains:
					baseSprite.setColor(sf::Color(200, 238, 124, 255));
					break;
				case forests:
					baseSprite.setColor(sf::Color(0, 145, 0, 255));
					break;
				case mountains:
					baseSprite.setColor(sf::Color(159, 159, 159, 255));
					break;
				case hills:
					baseSprite.setColor(sf::Color(183, 186, 120, 255));
					break;
				case marshes:
					baseSprite.setColor(sf::Color(19, 145, 110, 255));
					break;
				case water:
					baseSprite.setColor(sf::Color(68, 132, 255, 255));
					break;
				default:
					baseSprite.setColor(sf::Color(255, 255, 255, 255));
				}
				window->draw(baseSprite);
				switch (map->hexagons[i][j].getNationality())
				{
				case red:
					nationSprite.setPosition(sf::Vector2f(nationSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
						nationSprite.getGlobalBounds().height * i - scrollOffset.y));
					if (j % 2 == 1) nationSprite.move(sf::Vector2f(0, hexOffset));
					nationSprite.setColor(sf::Color(255, 0, 0, 255));
					window->draw(nationSprite);
					break;
				case green:
					nationSprite.setPosition(sf::Vector2f(nationSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
						nationSprite.getGlobalBounds().height * i - scrollOffset.y));
					if (j % 2 == 1) nationSprite.move(sf::Vector2f(0, hexOffset));
					nationSprite.setColor(sf::Color(0, 255, 0, 255));
					window->draw(nationSprite);
					break;
				case blue:
					nationSprite.setPosition(sf::Vector2f(nationSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
						nationSprite.getGlobalBounds().height * i - scrollOffset.y));
					if (j % 2 == 1) nationSprite.move(sf::Vector2f(0, hexOffset));
					nationSprite.setColor(sf::Color(0, 0, 255, 255));
					window->draw(nationSprite);
					break;
				case yellow:
					nationSprite.setPosition(sf::Vector2f(nationSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
						nationSprite.getGlobalBounds().height * i - scrollOffset.y));
					if (j % 2 == 1) nationSprite.move(sf::Vector2f(0, hexOffset));
					nationSprite.setColor(sf::Color(255, 255, 0, 255));
					window->draw(nationSprite);
					break;
				default:
					break;
				}
				if (map->hexagons[i][j].infrastructureLvl > 0)
				{
					infra2Sprite.setPosition(sf::Vector2f(infra2Sprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
						infra2Sprite.getGlobalBounds().height * i - scrollOffset.y));
					if (j % 2 == 1) infra2Sprite.move(sf::Vector2f(0, hexOffset));
					window->draw(infra2Sprite);
				}
				if (map->hexagons[i][j].resource.getType() != null)
					switch (map->hexagons[i][j].resource.getType())
					{
					case iron:
						ironSprite.setPosition(sf::Vector2f(ironSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
							ironSprite.getGlobalBounds().height * i - scrollOffset.y));
						if (j % 2 == 1) ironSprite.move(sf::Vector2f(0, hexOffset));
						window->draw(ironSprite);
						break;
					case food:
						foodSprite.setPosition(sf::Vector2f(foodSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
							foodSprite.getGlobalBounds().height * i - scrollOffset.y));
						if (j % 2 == 1) foodSprite.move(sf::Vector2f(0, hexOffset));
						window->draw(foodSprite);
						break;
					case coal:
						coalSprite.setPosition(sf::Vector2f(coalSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
							coalSprite.getGlobalBounds().height * i - scrollOffset.y));
						if (j % 2 == 1) coalSprite.move(sf::Vector2f(0, hexOffset));
						window->draw(coalSprite);
						break;
					case steel:
						steelSprite.setPosition(sf::Vector2f(steelSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
							steelSprite.getGlobalBounds().height * i - scrollOffset.y));
						if (j % 2 == 1) steelSprite.move(sf::Vector2f(0, hexOffset));
						window->draw(steelSprite);
						break;
					case wood:
						woodSprite.setPosition(sf::Vector2f(woodSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
							woodSprite.getGlobalBounds().height * i - scrollOffset.y));
						if (j % 2 == 1) woodSprite.move(sf::Vector2f(0, hexOffset));
						window->draw(woodSprite);
						break;
					case supply:
						supplySprite.setPosition(sf::Vector2f(supplySprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
							supplySprite.getGlobalBounds().height * i - scrollOffset.y));
						if (j % 2 == 1) supplySprite.move(sf::Vector2f(0, hexOffset));
						window->draw(supplySprite);
						break;
					default:
						break;
					}
				if (map->hexagons[i][j].townLvl > 0)
				{
					townSprite.setPosition(sf::Vector2f(townSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
						townSprite.getGlobalBounds().height * i - scrollOffset.y));
					if (j % 2 == 1) townSprite.move(sf::Vector2f(0, hexOffset));
					window->draw(townSprite);
				}
				if (map->hexagons[i][j].supplyDepotLvl > 0)
				{
					warehouseSprite.setPosition(sf::Vector2f(warehouseSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
						warehouseSprite.getGlobalBounds().height * i - scrollOffset.y));
					if (j % 2 == 1) warehouseSprite.move(sf::Vector2f(0, hexOffset));
					window->draw(warehouseSprite);
				}
				if (map->hexagons[i][j].militaryUnits.size() > 0)
				{
					//base units draw with selection
					int offset = 0;
					int selInd = map->hexagons[i][j].selectedMilUnit;
					for (int k = 0; k < map->hexagons[i][j].militaryUnits.size(); k++)
					{
						milUnitSprite.setPosition(sf::Vector2f(milUnitSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x + offset,
							milUnitSprite.getGlobalBounds().height * i - scrollOffset.y));
						if (j % 2 == 1) milUnitSprite.move(sf::Vector2f(0, hexOffset));
						if (k == selInd)
						{
							milUnitSprite.setColor(sf::Color::Yellow);
							window->draw(milUnitSprite);
							milUnitSprite.setColor(sf::Color::White);
						}
						else
							window->draw(milUnitSprite);
						offset += 5;
						//progress bar for move drawing
						if (map->hexagons[i][j].militaryUnits[k].moveProgress > -1)//is moving
						{
							//set base
							unitMove->setProgress(map->hexagons[i][j].militaryUnits[k].moveProgress);
							if (j % 2 == 0)
								unitMove->setPos(sf::Vector2f(sf::Vector2f(baseSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
									baseSprite.getGlobalBounds().height * i - scrollOffset.y)));
							else if (j % 2 == 1)
								unitMove->setPos(sf::Vector2f(sf::Vector2f(baseSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
									baseSprite.getGlobalBounds().height * i - scrollOffset.y + hexOffset)));
							//set angle
							//up
							if (map->hexagons[i][j].militaryUnits[k].road.back().x == map->hexagons[i][j].getPos().y &&
								map->hexagons[i][j].militaryUnits[k].road.back().y == map->hexagons[i][j].getPos().x - 1)
							{
								unitMove->setRotation(270);
								unitMove->translate(sf::Vector2f(270 * globalScale->x, 100 * globalScale->y));
							}
							//down
							if (map->hexagons[i][j].militaryUnits[k].road.back().x == map->hexagons[i][j].getPos().y &&
								map->hexagons[i][j].militaryUnits[k].road.back().y == map->hexagons[i][j].getPos().x + 1)
							{
								unitMove->setRotation(90);
								unitMove->translate(sf::Vector2f(320 * globalScale->x, 400 * globalScale->y));
							}
							else unitMove->setRotation(0);
							//std::cout << map->hexagons[i][j].militaryUnits[k].road.back().x;
							window->draw(*unitMove);
						}
					}
				}
				if (map->hexagons[i][j].logisticUnits.size() > 0)
				{
					int offset = 0;
					int selInd = map->hexagons[i][j].selectedLogUnit;
					for (int k = 0; k < map->hexagons[i][j].logisticUnits.size(); k++)
					{
						//base units draw with selection
						logUnitSprite.setPosition(sf::Vector2f(logUnitSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x + offset,
							logUnitSprite.getGlobalBounds().height * i - scrollOffset.y));
						if (j % 2 == 1) logUnitSprite.move(sf::Vector2f(0, hexOffset));
						if (k == selInd)
						{
							logUnitSprite.setColor(sf::Color::Yellow);
							window->draw(logUnitSprite);
							logUnitSprite.setColor(sf::Color::White);
						}
						else
							window->draw(logUnitSprite);
						offset += 5;
						//progress bar for move drawing
						if (map->hexagons[i][j].logisticUnits[k].moveProgress > -1)//is moving
						{
							unitMove->setProgress(map->hexagons[i][j].logisticUnits[k].moveProgress);
							if (j % 2 == 0)
								unitMove->setPos(sf::Vector2f(sf::Vector2f(baseSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
									baseSprite.getGlobalBounds().height * i - scrollOffset.y)));
							else if (j % 2 == 1)
								unitMove->setPos(sf::Vector2f(sf::Vector2f(baseSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
									baseSprite.getGlobalBounds().height * i - scrollOffset.y + hexOffset)));
							window->draw(*unitMove);
						}
					}
				}
				if (map->hexagons[i][j].factory != nullptr)
				{
					factorySprite.setPosition(sf::Vector2f(factorySprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
						factorySprite.getGlobalBounds().height * i - scrollOffset.y));
					if (j % 2 == 1) factorySprite.move(sf::Vector2f(0, hexOffset));
					window->draw(factorySprite);
				}
				if (map->hexagons[i][j].selected == true)
				{
					baseSprite.setColor(sf::Color(255, 255, 255, 60));
					window->draw(baseSprite);
				}
			}
			else
			{
				baseSprite.setPosition(sf::Vector2f(baseSprite.getGlobalBounds().width*0.75f * j - scrollOffset.x,
					baseSprite.getGlobalBounds().height * i - scrollOffset.y));
				if (j % 2 == 1) baseSprite.move(sf::Vector2f(0, hexOffset));
				baseSprite.setColor(sf::Color(80, 80, 80, 255));
				window->draw(baseSprite);
			}
		}
	}
}

void GameFrame::drawMenu()
{
	window->draw(menuShade);
	window->draw(menuBackground);
	window->draw(*continueButton);
	window->draw(*saveButton);
	window->draw(*exitButton);
}

void GameFrame::drawUI()
{
	window->draw(uiBackground);
}

void GameFrame::drawHexInfo(sf::Vector2i pos)
{
	selectedCoordsLabel.setString("Hexagon position \nX: " + std::to_string(pos.x) + " Y: " + std::to_string(pos.y));
	window->draw(selectedCoordsLabel);
	window->draw(*infrastructureButton);
	window->draw(*townButton);
	window->draw(*supplyDepotButton);
	window->draw(*factoryButton);
	window->draw(*unitsButton);
}

void GameFrame::drawEntities(sf::Vector2i pos)
{
	window->draw(entitiesShade);
	window->draw(entitiesBackground);
	window->draw(*cancelButton);
	if (eMode != eUnits && eMode != eNull && eMode != eFactory)
		window->draw(*upgradeButton);

	if (eMode == eSupplies)
	{
		goodsLabel.setString("Goods:\n");
		goodsLabel.setString(goodsLabel.getString() + "Food: " + std::to_string(map->hexagons[selectedV.y][selectedV.x].getResource(food)) + "\n");
		goodsLabel.setString(goodsLabel.getString() + "Wood: " + std::to_string(map->hexagons[selectedV.y][selectedV.x].getResource(wood)) + "\n");
		goodsLabel.setString(goodsLabel.getString() + "Coal: " + std::to_string(map->hexagons[selectedV.y][selectedV.x].getResource(coal)) + "\n");
		goodsLabel.setString(goodsLabel.getString() + "Iron: " + std::to_string(map->hexagons[selectedV.y][selectedV.x].getResource(iron)) + "\n");
		//for (int i = 0; i < map->hexagons[selectedV.y][selectedV.x].depotResources.size(); i++)
		//{
		//	if (map->hexagons[selectedV.y][selectedV.x].depotResources[i].getAmmount() > 0)
		//		goodsLabel.setString(goodsLabel.getString() +
		//			map->hexagons[selectedV.y][selectedV.x].depotResources[i].getString() + "\n");
		//}
		window->draw(goodsLabel);
	}
	else if (eMode == eFactory)
	{
		if (map->hexagons[selectedV.y][selectedV.x].factory == nullptr)
		{
			if (map->hexagons[selectedV.y][selectedV.x].resource.getType() != null)
			{
				factBack[0].setPos(sf::Vector2f(entitiesBackground.getPosition().x + 50.f, entitiesBackground.getPosition().y + 100.f));
				window->draw(factBack[0]);
				switch (map->hexagons[selectedV.y][selectedV.x].resource.getType())
				{
				case coal:
					coalSprite.setPosition(sf::Vector2f(factBack[0].getPosition().x - 15.f, factBack[0].getPosition().y - 50.f));
					coalSprite.setScale(sf::Vector2f(0.2f, 0.2f));
					window->draw(coalSprite);
					updateScales();
					break;
				case null:
					//factBack->setPosition(sf::Vector2f(entitiesBackground.getPosition().x + 50.f, entitiesBackground.getPosition().y + 100.f));
					//window->draw(*factBack);
					break;
				}
			}
			else
			{
				factBack[1].setPos(sf::Vector2f(entitiesBackground.getPosition().x + 110.f, entitiesBackground.getPosition().y + 100.f));
				factBack[2].setPos(sf::Vector2f(entitiesBackground.getPosition().x + 170.f, entitiesBackground.getPosition().y + 100.f));
				window->draw(factBack[1]);
				window->draw(factBack[2]);
				steelSprite.setPosition(sf::Vector2f(factBack[1].getPosition().x - 15.f, factBack[1].getPosition().y - 50.f));
				steelSprite.setScale(sf::Vector2f(0.2f, 0.2f));
				window->draw(steelSprite);
				supplySprite.setPosition(sf::Vector2f(factBack[2].getPosition().x - 15.f, factBack[2].getPosition().y - 50.f));
				supplySprite.setScale(sf::Vector2f(0.2f, 0.2f));
				window->draw(supplySprite);
				updateScales();
				// if resource present dedicated mine
				//else if no resource and plains further factory processing stages
			}
		}
		else
		{
			//show factory stocks and stats and upgrade button
			window->draw(*upgradeButton);
		}
	}
}
