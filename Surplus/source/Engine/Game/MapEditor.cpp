#include <fstream>
#include "MapEditor.h"
#include "../../Fields/Factory.h"
#include "../../Units/MilitaryUnit.h"
#include "../../Units/LogisticUnit.h"
#include "../../Fields/nationality.h"
#include "../Gui/Button.h"
#include "../Gui/TextBox.h"
#include "../Gui/DropDown.h"
#include "../Map.h"
#include "../../Fields/Hexagon.h"
#include "../../stance.h"


MapEditor::MapEditor(sf::RenderWindow* window, stance* mainStance, Map* map) : GameFrame(window, mainStance)
{
	this->map = map;

	//max scroll map distances
	maxScrollY = int((baseSprite.getGlobalBounds().height * map->getSize().y)
		+ hexOffset - window->getSize().y - 10);
	maxScrollX = int((baseSprite.getGlobalBounds().width*0.7501f) * map->getSize().x - window->getSize().x + 10);
	//ui maxScrollX increase
	maxScrollX += window->getSize().x / 6;

	initializeMenu();
	initializeUi();
	initializeEntities();
	initializeEditingButtons();

	proceduralMapGeneration();
	for (int i = 0; i < map->getSize().y; i++)
		for (int j = 0; j < map->getSize().x; j++)
			map->hexagons[i][j].visible = true;
}

MapEditor::~MapEditor()
{
	delete terrainToFieldsButton;
	delete terrainToForestsButton;
	delete terrainToMountainsButton;
	delete terrainToHillsButton;
	delete terrainToMarshesButton;
	delete terrainToWaterButton;
	delete nationalityToRedButton;
	delete nationalityToGreenButton;
	delete nationalityToBlueButton;
	delete nationalityToYellowButton;
	delete resourceChangeButton;
	delete depoResourceDropDown;
	delete resourcesTextBox;
	delete depoResourceAddButton;
}

void MapEditor::initializeMenu()
{
	continueButton->setString("Continue Map Edition");
	saveButton->setString("Save Map");
	saveToFileButton->setString("Save Map");
}

void MapEditor::initializeUi()
{

}

void MapEditor::initializeEditingButtons()
{
	//terrains
	terrainToFieldsButton = new gui::Button("Fields", sf::Vector2f(uiBackground.getPosition().x + 5,
		terrainLabel.getPosition().y + terrainLabel.getCharacterSize() * 2.f), sf::Vector2f(60.f, 50.f), &clickSound);
	terrainToForestsButton = new gui::Button("Forests", sf::Vector2f(terrainToFieldsButton->getPosition().x + 5 + terrainToFieldsButton->getSize().x,
		terrainToFieldsButton->getPosition().y), sf::Vector2f(80.f, 50.f), &clickSound);
	terrainToMountainsButton = new gui::Button("Mountains", sf::Vector2f(terrainToForestsButton->getPosition().x + 5 + terrainToForestsButton->getSize().x,
		terrainToFieldsButton->getPosition().y), sf::Vector2f(100.f, 50.f), &clickSound);
	terrainToHillsButton = new gui::Button("Hills", sf::Vector2f(uiBackground.getPosition().x + 5,
		terrainToFieldsButton->getPosition().y + 5 + terrainToFieldsButton->getSize().y), sf::Vector2f(60.f, 50.f), &clickSound);
	terrainToMarshesButton = new gui::Button("Marshes", sf::Vector2f(terrainToHillsButton->getPosition().x + 5 + terrainToHillsButton->getSize().x,
		terrainToHillsButton->getPosition().y), sf::Vector2f(80.f, 50.f), &clickSound);
	terrainToWaterButton = new gui::Button("Water", sf::Vector2f(terrainToMarshesButton->getPosition().x + 5 + terrainToMarshesButton->getSize().x,
		terrainToHillsButton->getPosition().y), sf::Vector2f(100.f, 50.f), &clickSound);
	//nationalities
	nationalityToRedButton = new gui::Button("Red", sf::Vector2f(uiBackground.getPosition().x + 5,
		nationalityLabel.getPosition().y + nationalityLabel.getCharacterSize() * 2.f), sf::Vector2f(60.f, 50.f), &clickSound);
	nationalityToGreenButton = new gui::Button("Green", sf::Vector2f(uiBackground.getPosition().x + 10 + nationalityToRedButton->getSize().x,
		nationalityLabel.getPosition().y + nationalityLabel.getCharacterSize() * 2.f), sf::Vector2f(60.f, 50.f), &clickSound);
	nationalityToBlueButton = new gui::Button("Blue", sf::Vector2f(uiBackground.getPosition().x + 15 + nationalityToRedButton->getSize().x + nationalityToGreenButton->getSize().x,
		nationalityLabel.getPosition().y + nationalityLabel.getCharacterSize() * 2.f), sf::Vector2f(60.f, 50.f), &clickSound);
	nationalityToYellowButton = new gui::Button("Yellow", sf::Vector2f(uiBackground.getPosition().x + 20 + nationalityToRedButton->getSize().x
		+ nationalityToGreenButton->getSize().x + nationalityToBlueButton->getSize().x,
		nationalityLabel.getPosition().y + nationalityLabel.getCharacterSize() * 2.f), sf::Vector2f(60.f, 50.f), &clickSound);
	//resources
	Resource r = Resource();
	resourceChangeDropDown = new gui::DropDown(r.getResourceAmmount() - 3, r.getResourceTypes(), sf::Vector2f(uiBackground.getPosition().x + 5,
		resourceLabel.getPosition().y + resourceLabel.getCharacterSize() * 2.f), sf::Vector2f(100.f, 40.f), &listSound);
	resourceChangeButton = new gui::Button("Resource", sf::Vector2f(uiBackground.getPosition().x + 135.f,
		resourceLabel.getPosition().y + resourceLabel.getCharacterSize() * 2.f), sf::Vector2f(80.f, 40.f), &clickSound);
}

void MapEditor::initializeEntities()
{
	Resource r = Resource();
	//
	depoResourceDropDown = new gui::DropDown(r.getResourceAmmount(), r.getResourceTypes(), sf::Vector2f(entitiesBackground.getPosition().x + 225.f,
		entitiesBackground.getPosition().y + 20.f), &listSound);
	resourcesTextBox = new gui::TextBox(sf::Vector2f(entitiesBackground.getPosition().x + 225.f, entitiesBackground.getPosition().y + 60.f),
		sf::Vector2f(100.f, 40.f));
	resourcesTextBox->setNumbersOnly(true);
	resourcesTextBox->maxValue = 65535;
	depoResourceAddButton = new gui::Button("Add", sf::Vector2f(entitiesBackground.getPosition().x + 225.f,
		entitiesBackground.getPosition().y + 110), sf::Vector2f(60.f, 50.f), &clickSound);
	depoResourceDeleteButton = new gui::Button("Delete", sf::Vector2f(depoResourceAddButton->getPosition().x + 70.f,
		entitiesBackground.getPosition().y + 110), sf::Vector2f(60.f, 50.f), &clickSound);
}

void MapEditor::processGameInput(sf::Event event)
{
	GameFrame::processGameInput(event);

	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (selected && worldPos.x >= uiBackground.getPosition().x)
	{
		resourceChangeDropDown->processInput(worldPos, &event);
		if (terrainToFieldsButton->processInput(worldPos, event))
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == plains)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
			else
				map->hexagons[selectedV.y][selectedV.x].setTerrain(plains);
		else if (terrainToForestsButton->processInput(worldPos, event))
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == forests)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
			else
				map->hexagons[selectedV.y][selectedV.x].setTerrain(forests);
		else if (terrainToMountainsButton->processInput(worldPos, event))
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == mountains)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
			else
				map->hexagons[selectedV.y][selectedV.x].setTerrain(mountains);
		else if (terrainToWaterButton->processInput(worldPos, event))
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == water)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
			else
			{
				map->hexagons[selectedV.y][selectedV.x].setTerrain(water);
				map->hexagons[selectedV.y][selectedV.x].setNationality(noNationality);
				map->hexagons[selectedV.y][selectedV.x].resource.setType(null);
			}
		else if (terrainToHillsButton->processInput(worldPos, event))
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == hills)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
			else
				map->hexagons[selectedV.y][selectedV.x].setTerrain(hills);
		else if (terrainToMarshesButton->processInput(worldPos, event))
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == marshes)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
			else
				map->hexagons[selectedV.y][selectedV.x].setTerrain(marshes);
		else if (nationalityToRedButton->processInput(worldPos, event))
		{
			if (map->hexagons[selectedV.y][selectedV.x].getNationality() == red)
				map->hexagons[selectedV.y][selectedV.x].setNationality(noNationality);
			else
				map->hexagons[selectedV.y][selectedV.x].setNationality(red);
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == water)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
		}
		else if (nationalityToGreenButton->processInput(worldPos, event))
		{
			if (map->hexagons[selectedV.y][selectedV.x].getNationality() == green)
				map->hexagons[selectedV.y][selectedV.x].setNationality(noNationality);
			else
				map->hexagons[selectedV.y][selectedV.x].setNationality(green);
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == water)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
		}
		else if (nationalityToBlueButton->processInput(worldPos, event))
		{
			if (map->hexagons[selectedV.y][selectedV.x].getNationality() == blue)
				map->hexagons[selectedV.y][selectedV.x].setNationality(noNationality);
			else
				map->hexagons[selectedV.y][selectedV.x].setNationality(blue);
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == water)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
		}
		else if (nationalityToYellowButton->processInput(worldPos, event))
		{
			if (map->hexagons[selectedV.y][selectedV.x].getNationality() == yellow)
				map->hexagons[selectedV.y][selectedV.x].setNationality(noNationality);
			else
				map->hexagons[selectedV.y][selectedV.x].setNationality(yellow);
			if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == water)
				map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
		}
		else if (resourceChangeButton->processInput(worldPos, event))
		{
			if (resourceChangeDropDown->getSelectedIndex() != -1)
			{
				if (map->hexagons[selectedV.y][selectedV.x].resource.getType() == resourceChangeDropDown->getSelectedIndex() + 1)
					map->hexagons[selectedV.y][selectedV.x].resource.setType(null);
				else
				{
					map->hexagons[selectedV.y][selectedV.x].resource.setType((TypeOfResource)(resourceChangeDropDown->getSelectedIndex() + 1));
					map->hexagons[selectedV.y][selectedV.x].resource.setAmmount(-1);
				}
				if (map->hexagons[selectedV.y][selectedV.x].getTerrain() == water)
					map->hexagons[selectedV.y][selectedV.x].setTerrain(noTerrain);
			}
		}
		else if (infrastructureButton->processInput(worldPos, event))
		{
			if (map->hexagons[selectedV.y][selectedV.x].infrastructureLvl >= 0 && map->hexagons[selectedV.y][selectedV.x].infrastructureLvl < 2)
				map->hexagons[selectedV.y][selectedV.x].infrastructureLvl++;
			else
				map->hexagons[selectedV.y][selectedV.x].infrastructureLvl = 0;
		}
		else if (townButton->processInput(worldPos, event))
		{
			if (map->hexagons[selectedV.y][selectedV.x].townLvl >= 0 && map->hexagons[selectedV.y][selectedV.x].townLvl < 3)
				map->hexagons[selectedV.y][selectedV.x].townLvl++;
			else
				map->hexagons[selectedV.y][selectedV.x].townLvl = 0;
		}
		else if (supplyDepotButton->processInput(worldPos, event))
		{
			entitiesOn = true;
			eMode = eSupplies;
		}
		else if (factoryButton->processInput(worldPos, event))
		{
			entitiesOn = true;
			eMode = eFactory;
		}
		else if (unitsButton->processInput(worldPos, event))
		{
			entitiesOn = true;
			eMode = eUnits;
		}
	}
}

void MapEditor::processMenuInput(sf::Event event)
{
	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (!savingState)
	{
		if (continueButton->processInput(worldPos, event))
			menuOn = false;
		else if (saveButton->processInput(worldPos, event))
			savingState = true;
		else if (exitButton->processInput(worldPos, event))
		{
			//this->map->~Map();
			*mainStance = sMainMenu;
		}
	}
	else
	{
		if (saveToFileButton->processInput(worldPos, event) && textBox->getInput() != "")
		{
			std::ofstream outfile(".\\saves\\" + std::string(textBox->getInput()) + ".map", std::ofstream::binary);
			outfile.write(map->serialize(), 4 + map->getSize().x * map->getSize().y * 14); ///////////
			*mainStance = sMainMenu;
		}
		if (returnButton->processInput(worldPos, event))
		{
			savingState = false;
			textBox->reset();
		}
		//textBox->processText(event);
	}

	//mouse click	
	if (savingState)
		textBox->processInput(worldPos, event);
}

void MapEditor::processEntitiesInput(sf::Event event)
{
	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (cancelButton->processInput(worldPos, event))
	{
		entitiesOn = false;
		depoResourceDropDown->reset();
		resourcesTextBox->reset();
		eMode = eNull;
		return;
	}
	switch (eMode)
	{
	case eNull:
		break;
	case eSupplies:
		if (upgradeButton->processInput(worldPos, event))
		{
			map->hexagons[selectedV.y][selectedV.x].supplyDepotLvl++;
			if (map->hexagons[selectedV.y][selectedV.x].supplyDepotLvl > 3)
				map->hexagons[selectedV.y][selectedV.x].supplyDepotLvl = 0;
		}
		depoResourceDropDown->processInput(worldPos, &event);
		resourcesTextBox->processInput(worldPos, event);
		if (depoResourceAddButton->processInput(worldPos, event) && resourcesTextBox->getIntInput() > 0 &&
			depoResourceDropDown->getSelectedIndex() != -1)
		{
			switch (depoResourceDropDown->getSelectedIndex() + 1)
			{
			case iron:
				map->hexagons[selectedV.y][selectedV.x].changeResource(iron, resourcesTextBox->getIntInput());
				break;
			case food:
				map->hexagons[selectedV.y][selectedV.x].changeResource(food, resourcesTextBox->getIntInput());
				break;
			case coal:
				map->hexagons[selectedV.y][selectedV.x].changeResource(coal, resourcesTextBox->getIntInput());
				break;
			case steel:
				map->hexagons[selectedV.y][selectedV.x].changeResource(steel, resourcesTextBox->getIntInput());
				break;
			case wood:
				map->hexagons[selectedV.y][selectedV.x].changeResource(wood, resourcesTextBox->getIntInput());
				break;
			case supply:
				map->hexagons[selectedV.y][selectedV.x].changeResource(supply, resourcesTextBox->getIntInput());
				break;
			case manpower:
				map->hexagons[selectedV.y][selectedV.x].changeResource(manpower, resourcesTextBox->getIntInput());
				break;
			default:
				break;
			}
			resourcesTextBox->reset();
		}
		else if (depoResourceDeleteButton->processInput(worldPos, event) && depoResourceDropDown->getSelectedIndex() != -1)
		{
			switch (depoResourceDropDown->getSelectedIndex() + 1)
			{
			case iron:
				map->hexagons[selectedV.y][selectedV.x].changeResource(iron, -resourcesTextBox->getIntInput());
				break;
			case food:
				map->hexagons[selectedV.y][selectedV.x].changeResource(food, -resourcesTextBox->getIntInput());
				break;
			case coal:
				map->hexagons[selectedV.y][selectedV.x].changeResource(coal, -resourcesTextBox->getIntInput());
				break;
			case steel:
				map->hexagons[selectedV.y][selectedV.x].changeResource(steel, -resourcesTextBox->getIntInput());
				break;
			case wood:
				map->hexagons[selectedV.y][selectedV.x].changeResource(wood, -resourcesTextBox->getIntInput());
				break;
			case supply:
				map->hexagons[selectedV.y][selectedV.x].changeResource(supply, -resourcesTextBox->getIntInput());
				break;
			case manpower:
				map->hexagons[selectedV.y][selectedV.x].changeResource(manpower, -resourcesTextBox->getIntInput());
				break;
			default:
				break;
			}
			resourcesTextBox->reset();
		}
		break;
	case eFactory:
		if (map->hexagons[selectedV.y][selectedV.x].factory == nullptr)
		{
			if (map->hexagons[selectedV.y][selectedV.x].resource.getType() != null)
			{
				if (factBack[0].processInput(worldPos, event))
				{
					map->hexagons[selectedV.y][selectedV.x].factory = new Factory(coal);
					map->hexagons[selectedV.y][selectedV.x].factory->setLevel(1);
				}
			}
			else
			{
				if (factBack[1].processInput(worldPos, event))
				{
					map->hexagons[selectedV.y][selectedV.x].factory = new Factory(steel);
					map->hexagons[selectedV.y][selectedV.x].factory->setLevel(1);
					map->hexagons[selectedV.y][selectedV.x].resource = Resource(steel, -1);
				}
				if (factBack[2].processInput(worldPos, event))
				{
					map->hexagons[selectedV.y][selectedV.x].factory = new Factory(supply);
					map->hexagons[selectedV.y][selectedV.x].factory->setLevel(1);
					map->hexagons[selectedV.y][selectedV.x].resource = Resource(supply, -1);
				}
			}
		}
		else
		{
			if (upgradeButton->processInput(worldPos, event))
			{
				map->hexagons[selectedV.y][selectedV.x].factory->setLevel(map->hexagons[selectedV.y][selectedV.x].factory->getLevel() + 1);
				if (map->hexagons[selectedV.y][selectedV.x].factory->getLevel() > 5)
				{
					map->hexagons[selectedV.y][selectedV.x].factory->~Factory();
					map->hexagons[selectedV.y][selectedV.x].factory = nullptr;
					if (map->hexagons[selectedV.y][selectedV.x].resource.getType() == supply ||
						map->hexagons[selectedV.y][selectedV.x].resource.getType() == steel)
						map->hexagons[selectedV.y][selectedV.x].resource.setType(null);
				}
			}
		}
		break;
	case eUnits:
		if (milAddButton->processInput(worldPos, event))
		{
			map->hexagons[selectedV.y][selectedV.x].militaryUnits.push_back(MilitaryUnit());
			if (map->hexagons[selectedV.y][selectedV.x].militaryUnits.size() > 3)
				map->hexagons[selectedV.y][selectedV.x].militaryUnits.clear();
		}
		else if (logAddButton->processInput(worldPos, event))
		{
			map->hexagons[selectedV.y][selectedV.x].logisticUnits.push_back(LogisticUnit());
			if (map->hexagons[selectedV.y][selectedV.x].logisticUnits.size() > 3)
				map->hexagons[selectedV.y][selectedV.x].logisticUnits.clear();
		}
		break;
	case eCity:
		break;
	case eInfrastructure:
		break;
	default:
		eMode = eNull;
		break;
	}
}

void MapEditor::mainLoop()
{
	while (*mainStance == sMapEdit)
	{
		//process events and inputs
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (!menuOn && eMode == eNull)
				processGameInput(event);
			else if (menuOn)
				processMenuInput(event);
			else if (!menuOn && eMode != eNull)
				processEntitiesInput(event);
			//global shortcuts
			{
				if ((event.type == sf::Event::KeyReleased) && (event.key.code == sf::Keyboard::Escape))
				{
					menuOn = !menuOn;
					savingState = false;
					textBox->reset();
				}
				if (event.type == sf::Event::Closed)
				{
					*mainStance = sExit;
					return;
				}
			}
		}
		scrollHandle();
		drawingMainScreen();
		drawUI();
		if (selected)
			drawHexInfo(selectedV);
		if (entitiesOn)
			drawEntities(selectedV);
		if (menuOn)
			drawMenu();
		window->display();
	}
}

void MapEditor::drawMenu()
{
	GameFrame::drawMenu();
	if (savingState)
	{
		window->draw(saveBackground);
		window->draw(*saveToFileButton);
		window->draw(*textBox);
		window->draw(*returnButton);
	}
}

void MapEditor::drawUI()
{
	GameFrame::drawUI();
}

void MapEditor::drawHexInfo(sf::Vector2i pos)
{
	GameFrame::drawHexInfo(pos);

	if (map->hexagons[pos.y][pos.x].getTerrain() > 0)
	{
		terrainLabel.setString("Terrain type: " + terrainType_str[map->hexagons[pos.y][pos.x].getTerrain()]);
		window->draw(terrainLabel);
	}
	if (map->hexagons[pos.y][pos.x].getNationality() > 0)
	{
		nationalityLabel.setString("Nationality: " + nationalities_str[map->hexagons[pos.y][pos.x].getNationality()]);
		window->draw(nationalityLabel);
	}
	if (map->hexagons[pos.y][pos.x].resource.getType() != null)
	{
		resourceLabel.setString("Resource: " + map->hexagons[pos.y][pos.x].resource.getString());
		window->draw(resourceLabel);
	}
	window->draw(*terrainToFieldsButton);
	window->draw(*terrainToForestsButton);
	window->draw(*terrainToMountainsButton);
	window->draw(*terrainToWaterButton);
	window->draw(*terrainToHillsButton);
	window->draw(*terrainToMarshesButton);
	window->draw(*nationalityToRedButton);
	window->draw(*nationalityToGreenButton);
	window->draw(*nationalityToBlueButton);
	window->draw(*nationalityToYellowButton);
	window->draw(*resourceChangeButton);
	//if (map->hexagons[pos.y][pos.x].getNationality() > 0)
	//{
	entitiesLabel.setString("Entities:\nInfrastructure lvl: " + std::to_string(map->hexagons[pos.y][pos.x].infrastructureLvl) +
		"\nSupply depot lvl: " + std::to_string(map->hexagons[pos.y][pos.x].supplyDepotLvl) +
		"\nCity lvl: " + std::to_string(map->hexagons[pos.y][pos.x].townLvl) +
		"\nFactory lvl: " + map->hexagons[pos.y][pos.x].factory->showLevel());
	window->draw(entitiesLabel);
	//}
	window->draw(*resourceChangeDropDown);

}

void MapEditor::drawEntities(sf::Vector2i pos)
{
	GameFrame::drawEntities(pos);

	if (eMode == eUnits)
	{
		window->draw(*milAddButton);
		milLabel.setString("Military units: " + std::to_string(map->hexagons[selectedV.y][selectedV.x].militaryUnits.size()));
		window->draw(milLabel);
		window->draw(*logAddButton);
		logLabel.setString("Logistic units: " + std::to_string(map->hexagons[selectedV.y][selectedV.x].logisticUnits.size()));
		window->draw(logLabel);
	}
	if (eMode == eSupplies)
	{
		window->draw(*resourcesTextBox);
		window->draw(*depoResourceAddButton);
		window->draw(*depoResourceDeleteButton);
		window->draw(*depoResourceDropDown);
	}

}
