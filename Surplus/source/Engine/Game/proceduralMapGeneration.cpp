#pragma once
#include <cstdlib>
#include "MapEditor.h"
#include "../Map.h"
#include "../../Fields/Hexagon.h"

void MapEditor::proceduralMapGeneration()
{
	for (int i = 0; i < map->getSize().x; i++)
		for (int j = 0; j < map->getSize().y; j++)
		{
			int tab[] = { 10, 20, 30, 40, 50 };
			if (j > 0)
				setDynamicRange(j - 1, i, tab);  // up
			if (j < map->getSize().y - 1)
				setDynamicRange(j + 1, i, tab);  //down
			if (i % 2 == 1)
			{
				if (i < map->getSize().x - 1)
				{
					setDynamicRange(j, i + 1, tab);  // up right
					if (j < map->getSize().y - 1)
						setDynamicRange(j + 1, i + 1, tab); // down right
				}
				if (i > 0)
				{
					setDynamicRange(j, i - 1, tab);  // up left
					if (j < map->getSize().y - 1)
						setDynamicRange(j + 1, i - 1, tab); // down left	
				}
			}
			else if (i % 2 == 0)
			{
				if (i < map->getSize().x - 1 && j > 0)
				{
					setDynamicRange(j - 1, i + 1, tab);  // up right
					if (j < map->getSize().y - 1)
						setDynamicRange(j, i + 1, tab); // down right
				}
				if (i > 0 && j > 0)
				{
					setDynamicRange(j - 1, i - 1, tab);  // up left
					if (j < map->getSize().y - 1)
						setDynamicRange(j, i - 1, tab); // down left	
				}
			}

			//static differences:
			tab[0] += 3; tab[1] += 3; tab[2]; tab[3] += 2; tab[4] -= 2;
			int r = rand() % 60;
			if (r < tab[0])
				map->hexagons[j][i].setTerrain(plains);
			else if (r < tab[1])
				map->hexagons[j][i].setTerrain(forests);
			else if (r < tab[2])
				map->hexagons[j][i].setTerrain(mountains);
			else if (r < tab[3])
				map->hexagons[j][i].setTerrain(hills);
			else if (r < tab[4])
				map->hexagons[j][i].setTerrain(marshes);
			else
				map->hexagons[j][i].setTerrain(water);
		}

	//resources
	//if forest probability of wood resource
	for (int i = 0; i < ((map->getSize().y * map->getSize().x) / 65) + 1; i++)
	{
		int randX = rand() % map->getSize().x, randY = rand() % map->getSize().y;
		while (map->hexagons[randY][randX].getTerrain() != forests)
		{
			randX = rand() % map->getSize().x;
			randY = rand() % map->getSize().y;
		}
		map->hexagons[randY][randX].resource.setType(wood);
		map->hexagons[randY][randX].resource.setAmmount(-1);
	}
	//if plains probability of food resource
	for (int i = 0; i < ((map->getSize().y * map->getSize().x) / 50) + 1; i++)
	{
		int randX = rand() % map->getSize().x, randY = rand() % map->getSize().y;
		while (map->hexagons[randY][randX].getTerrain() != plains)
		{
			randX = rand() % map->getSize().x;
			randY = rand() % map->getSize().y;
		}
		map->hexagons[randY][randX].resource.setType(food);
		map->hexagons[randY][randX].resource.setAmmount(-1);
	}
	//coal
	for (int i = 0; i < ((map->getSize().y * map->getSize().x) / 100)+1; i++)
	{
		int randX = rand() % map->getSize().x, randY = rand() % map->getSize().y;
		//int ammount = rand() % 5;
		while (map->hexagons[randY][randX].getTerrain() == water)
		{
			randX = rand() % map->getSize().x;
			randY = rand() % map->getSize().y;
		}
		//while (ammount >= 0)
		//{
		//	map->
		//}
		map->hexagons[randY][randX].resource.setType(coal);
		map->hexagons[randY][randX].resource.setAmmount(-1);

	}
	//iron
	for (int i = 0; i < ((map->getSize().y * map->getSize().x) / 100) + 1; i++)
	{
		int randX = rand() % map->getSize().x, randY = rand() % map->getSize().y;
		while (map->hexagons[randY][randX].getTerrain() == water)
		{
			randX = rand() % map->getSize().x;
			randY = rand() % map->getSize().y;
		}
		map->hexagons[randY][randX].resource.setType(iron);
		map->hexagons[randY][randX].resource.setAmmount(-1);

	}

}
void MapEditor::setDynamicRange(int y, int x, int tab[6])
{
	if (map->hexagons[y][x].getTerrain() > 0)
	{
		if (map->hexagons[y][x].getTerrain() == 1) //is fields
		{
			tab[0] += 10; tab[1] += 9; tab[2] += 6; tab[3] += 4; tab[4] += 2;
		}
		else if (map->hexagons[y][x].getTerrain() == 2) //is forests
		{
			tab[0] -= 2; tab[1] += 8; tab[2] += 6; tab[3] += 4; tab[4] += 2;
		}
		else if (map->hexagons[y][x].getTerrain() == 3) //is mountains
		{
			tab[0] -= 4; tab[1] -= 8; tab[2] += 2; tab[3] += 6; tab[4] += 4;
		}
		else if (map->hexagons[y][x].getTerrain() == 4) //is hills
		{
			tab[0] -= 2; tab[1] -= 4; tab[2] -= 6; tab[3] += 4; tab[4] += 2;
		}
		else if (map->hexagons[y][x].getTerrain() == 5) //is marshes
		{
			tab[0] -= 2; tab[1] -= 4; tab[2] -= 6; tab[3] -= 8; tab[4] += 2;
		}
		else if (map->hexagons[y][x].getTerrain() == 6) //is water
		{
			tab[0] -= 2; tab[1] -= 4; tab[2] -= 6; tab[3] -= 8; tab[4] -= 10;
		}
	}
}