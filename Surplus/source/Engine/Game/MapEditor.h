#pragma once
#include "GameFrame.h"

namespace gui
{
	class Button;
	class TextBox;
	class DropDown;
}

class MapEditor :
	public GameFrame
{
private:
	//map editing buttons
	gui::Button *terrainToFieldsButton;
	gui::Button *terrainToForestsButton;
	gui::Button *terrainToMountainsButton;
	gui::Button *terrainToHillsButton;
	gui::Button *terrainToMarshesButton;
	gui::Button *terrainToWaterButton;
	gui::Button *nationalityToRedButton;
	gui::Button *nationalityToGreenButton;
	gui::Button *nationalityToBlueButton;
	gui::Button *nationalityToYellowButton;
	gui::Button *resourceChangeButton;
	gui::DropDown *resourceChangeDropDown;
	gui::TextBox *resourcesTextBox;
	gui::Button *depoResourceAddButton;
	gui::Button *depoResourceDeleteButton;
	gui::DropDown *depoResourceDropDown;

public:
	MapEditor(sf::RenderWindow*, stance*, Map*);
	~MapEditor();
	void initializeMenu();
	void initializeUi();
	void initializeEditingButtons();
	void initializeEntities();
	void processGameInput(sf::Event);
	void processMenuInput(sf::Event);
	void processEntitiesInput(sf::Event event);
	void mainLoop();
	void drawMenu();
	void drawUI();
	void drawHexInfo(sf::Vector2i);
	void drawEntities(sf::Vector2i);
	void proceduralMapGeneration();
	void setDynamicRange(int, int, int[]);
};

