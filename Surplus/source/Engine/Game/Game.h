#pragma once
#include "GameFrame.h"
#include "../../Fields/nationality.h"


class Game:
	public GameFrame
{
private:
	nationality playerNationality;
public:
	bool gamePaused;
	int gameTick;

	Game(sf::RenderWindow *, stance *, Save *, nationality);
	~Game();
	void initializeMenu();
	void initializeUi();
	void processGameInput(sf::Event);
	void processMenuInput(sf::Event);
	void processEntitiesInput(sf::Event event);
	void mainLoop();
	void drawMenu();
	void drawUI();
	void drawHexInfo(sf::Vector2i);
	void drawEntities(sf::Vector2i);
};

