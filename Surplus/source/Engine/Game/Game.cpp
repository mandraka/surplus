#include <fstream>
#include "Game.h"
#include "../../Fields/Factory.h"
#include "../Gui/Button.h"
#include "../Gui/TextBox.h"
#include "../Save.h"
#include "../../Fields/Hexagon.h"
#include "../../stance.h"


Game::Game(sf::RenderWindow * window, stance *mainStance, Save *save, nationality player) : GameFrame(window, mainStance)
{
	this->save = save;
	this->map = save->map;
	playerNationality = player;
	this->save->map->player = &playerNationality;
	gamePaused = false;
	gameTick = 0;

	//max scroll map distances
	maxScrollY = int((baseSprite.getGlobalBounds().height * map->getSize().y)
		+ hexOffset - window->getSize().y - 10);
	maxScrollX = int(baseSprite.getGlobalBounds().width*0.7501f * map->getSize().x - window->getSize().x + 10);
	//ui maxScrollX increase
	maxScrollX += window->getSize().x / 6;

	initializeMenu();
	initializeUi();
	initializeEntities();

	// unhiding whole map //
	//for (int i = 0; i < map->getSize().y; i++)
	//	for (int j = 0; j < map->getSize().x; j++)
	//		map->hexagons[i][j].visible = true;

	for (int i = 0; i < map->getSize().y; i++)
		for (int j = 0; j < map->getSize().x; j++)
		{
			if (map->hexagons[i][j].getNationality() == red)
			{
				scrollOffset = sf::Vector2f(baseSprite.getGlobalBounds().width * 0.7501f * j - (window->getSize().x - uiBackground.getGlobalBounds().width) / 2,
					baseSprite.getGlobalBounds().height * i - window->getSize().y / 2 + baseSprite.getGlobalBounds().height / 2);
				while (scrollOffset.x < 0)
					scrollOffset.x++;
				while (scrollOffset.y < 0)
					scrollOffset.y++;
				map->hexagons[i][j].visible = true;
				map->makeSurroundingsVisible(sf::Vector2i(j, i), 2);
			}
		}
}

Game::~Game()
{
}

void Game::initializeMenu()
{
}

void Game::initializeUi()
{
}

void Game::processGameInput(sf::Event event)
{
	GameFrame::processGameInput(event);

	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (selected)
	{
		if (map->hexagons[selectedV.y][selectedV.x].getNationality() == *map->player)
		{
			if (infrastructureButton->processInput(worldPos, event))
			{
			}
			else if (townButton->processInput(worldPos, event))
			{
			}
			else if (supplyDepotButton->processInput(worldPos, event))
			{
				entitiesOn = true;
				eMode = eSupplies;
				gamePaused = true;
			}
			else if (factoryButton->processInput(worldPos, event))
			{
				entitiesOn = true;
				eMode = eFactory;
				gamePaused = true;
			}
			else if (unitsButton->processInput(worldPos, event))
			{
				entitiesOn = true;
				eMode = eUnits;
				gamePaused = true;
			}
		}
	}
	if (event.type == sf::Event::KeyReleased)
		if (event.key.code == sf::Keyboard::Space)
			gamePaused = !gamePaused;

	//mouse click	
	if (event.type == sf::Event::MouseButtonReleased)
	{
		if (event.mouseButton.button == sf::Mouse::Left && selected)
		{
			if (worldPos.x < uiBackground.getPosition().x)
			{
				map->hexagons[map->lastSelected.y][map->lastSelected.x].selectedMilUnit = -1;
				map->hexagons[map->lastSelected.y][map->lastSelected.x].selectedMilUnit = -1;
			}
		}
		//unit movement
		if (event.mouseButton.button == sf::Mouse::Right)
		{
			if (selected)
			{
				if (worldPos.x < uiBackground.getPosition().x)
				{
					//basic square selection
					int selectedHexX = int((worldPos.x + scrollOffset.x) / (baseSprite.getGlobalBounds().width*0.75f));
					int selectedHexY;
					if ((int)((worldPos.x + scrollOffset.x) / (baseSprite.getGlobalBounds().width*0.75f)) % 2 == 0)
						selectedHexY = int((worldPos.y + scrollOffset.y) / baseSprite.getGlobalBounds().height);
					else
						selectedHexY = (int(((worldPos.y + scrollOffset.y) + baseSprite.getGlobalBounds().height / 2) / baseSprite.getGlobalBounds().height)) - 1;

					//hex diagonals magics
					bool parityHex = true;
					int localHexXPos = int(worldPos.x + scrollOffset.x) - (selectedHexX * int(baseSprite.getGlobalBounds().width*0.75f));
					int localHexYPos = int(worldPos.y + scrollOffset.y) - (selectedHexY * int(baseSprite.getGlobalBounds().height));
					if ((int)((worldPos.x + scrollOffset.x) / (baseSprite.getGlobalBounds().width*0.75f)) % 2 == 1)
					{
						localHexYPos -= int(baseSprite.getGlobalBounds().height / 2);
						parityHex = false;
					}
					if (localHexXPos + localHexYPos < 170 * baseSprite.getScale().x)
					{
						selectedHexX--;
						if (parityHex)
							selectedHexY--;
					}
					else if (localHexYPos - localHexXPos > 300 * baseSprite.getScale().x)
					{
						selectedHexX--;
						selectedHexY++;
						if (parityHex)
							selectedHexY--;
					}
					//validation
					if (!(selectedHexY >= map->getSize().y) && !(selectedHexY < 0) && !(selectedHexX >= map->getSize().x) &&
						!(selectedHexX < 0) && map->hexagons[selectedHexY][selectedHexX].visible && map->hexagons[selectedHexY][selectedHexX].passable)
					{
						if (map->hexagons[selectedV.y][selectedV.x].selectedMilUnit > -1)
						{
							map->hexagons[selectedV.y][selectedV.x].militaryUnits
								[map->hexagons[selectedV.y][selectedV.x].selectedMilUnit].aStar(sf::Vector2i(selectedHexX, selectedHexY));

						}
						else if (map->hexagons[selectedV.y][selectedV.x].selectedLogUnit > -1)
						{

							if (map->hexagons[selectedHexY][selectedHexX].getNationality() != map->hexagons[selectedV.y][selectedV.x].getNationality())
								return;
							map->hexagons[selectedV.y][selectedV.x].logisticUnits
								[map->hexagons[selectedV.y][selectedV.x].selectedLogUnit].aStar(sf::Vector2i(selectedHexX, selectedHexY));
						}
					}
				}
			}
		}

	}
}

void Game::processMenuInput(sf::Event event)
{

	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);

	if (continueButton->processInput(worldPos, event))
	{
		menuOn = false;
		gamePaused = false;
	}
	else if (saveButton->processInput(worldPos, event))
	{
		std::ofstream outfile("new.save", std::ofstream::binary);
		//outfile.write(map->serialize(), sizeof(save));
	}
	else if (exitButton->processInput(worldPos, event))
		*mainStance = sMainMenu;
}

void Game::processEntitiesInput(sf::Event event)
{
	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (cancelButton->processInput(worldPos, event))
	{
		entitiesOn = false;
		eMode = eNull;
		gamePaused = false;
		return;
	}
	switch (eMode)
	{
	case eNull:
		break;
	case eSupplies:
		break;
	case eFactory:
		if (map->hexagons[selectedV.y][selectedV.x].factory == nullptr)
		{
			if (map->hexagons[selectedV.y][selectedV.x].resource.getType() != null)
			{
				if (factBack[0].processInput(worldPos, event))
				{
					map->hexagons[selectedV.y][selectedV.x].factory = new Factory(coal);
				}
			}
			else
			{
				if (factBack[1].processInput(worldPos, event))
				{
					map->hexagons[selectedV.y][selectedV.x].factory = new Factory(steel);
					map->hexagons[selectedV.y][selectedV.x].resource = Resource(steel, -1);
				}
				if (factBack[2].processInput(worldPos, event))
				{
					map->hexagons[selectedV.y][selectedV.x].factory = new Factory(supply);
					map->hexagons[selectedV.y][selectedV.x].resource = Resource(supply, -1);
				}
			}
		}
		else
		{
			if (upgradeButton->processInput(worldPos, event))
			{
				map->hexagons[selectedV.y][selectedV.x].factory->setLevel(map->hexagons[selectedV.y][selectedV.x].factory->getLevel() + 1);
				if (map->hexagons[selectedV.y][selectedV.x].factory->getLevel() > 5)
				{
					map->hexagons[selectedV.y][selectedV.x].factory->~Factory();
					map->hexagons[selectedV.y][selectedV.x].factory = nullptr;
				}
			}
		}
		break;
	case eUnits:
		if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
		{
			//units selection
			if (worldPos.x > 856 && worldPos.x < 935)
			{
				if (worldPos.y > 110 && worldPos.y < 160)
				{
					map->hexagons[selectedV.y][selectedV.x].selectedMilUnit = 0;
					map->hexagons[selectedV.y][selectedV.x].selectedLogUnit = -1;
				}
				if (worldPos.y > 160 && worldPos.y < 210)
				{
					map->hexagons[selectedV.y][selectedV.x].selectedMilUnit = 1;
					map->hexagons[selectedV.y][selectedV.x].selectedLogUnit = -1;
				}
				if (worldPos.y > 210 && worldPos.y < 260)
				{
					map->hexagons[selectedV.y][selectedV.x].selectedMilUnit = 2;
					map->hexagons[selectedV.y][selectedV.x].selectedLogUnit = -1;
				}
				if (worldPos.y > 270 && worldPos.y < 325)
				{
					map->hexagons[selectedV.y][selectedV.x].selectedMilUnit = -1;
					map->hexagons[selectedV.y][selectedV.x].selectedLogUnit = 0;
				}
				if (worldPos.y > 330 && worldPos.y < 385)
				{
					map->hexagons[selectedV.y][selectedV.x].selectedMilUnit = -1;
					map->hexagons[selectedV.y][selectedV.x].selectedLogUnit = 1;
				}
				if (worldPos.y > 390 && worldPos.y < 445)
				{
					map->hexagons[selectedV.y][selectedV.x].selectedMilUnit = -1;
					map->hexagons[selectedV.y][selectedV.x].selectedLogUnit = 2;
				}

			}
		}
		break;
	case eCity:
		break;
	case eInfrastructure:
		break;
	default:
		eMode = eNull;
		break;
	}
}

void Game::mainLoop()
{
	gamePaused = false;
	while (*mainStance == sGame)
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (!menuOn && eMode == eNull)
				processGameInput(event);
			else if (menuOn)
				processMenuInput(event);
			else if (!menuOn && eMode != eNull)
				processEntitiesInput(event);
			//global shortcuts
			{
				if ((event.type == sf::Event::KeyReleased) && (event.key.code == sf::Keyboard::Escape))
				{
					if (!menuOn)
						gamePaused = true;
					menuOn = !menuOn;
				}
				if (event.type == sf::Event::Closed)
				{
					*mainStance = sExit;
					return;
				}
			}
		}
		scrollHandle();
		if (!gamePaused) //update the world
		{
			gameTick++;
			if (gameTick > 30)
			{
				map->step();
				gameTick = 0;
			}
		}
		drawingMainScreen();
		drawUI();
		if (selected)
			drawHexInfo(selectedV);
		if (entitiesOn)
			drawEntities(selectedV);
		if (menuOn)
			drawMenu();
		window->display();
	}
}

void Game::drawMenu()
{
	GameFrame::drawMenu();
}

void Game::drawUI()
{
	GameFrame::drawUI();
}

void Game::drawHexInfo(sf::Vector2i pos)
{
	GameFrame::drawHexInfo(pos);

	if (map->hexagons[pos.y][pos.x].getTerrain() > 0 && map->hexagons[pos.y][pos.x].visible)
	{
		terrainLabel.setString("Terrain type: " + terrainType_str[map->hexagons[pos.y][pos.x].getTerrain()]);
		window->draw(terrainLabel);
	}
	if (map->hexagons[pos.y][pos.x].getNationality() > 0 && map->hexagons[pos.y][pos.x].visible)
	{
		nationalityLabel.setString("Nationality: " + nationalities_str[map->hexagons[pos.y][pos.x].getNationality()]);
		window->draw(nationalityLabel);
	}
	if (map->hexagons[pos.y][pos.x].resource.getType() != null && map->hexagons[pos.y][pos.x].visible)
	{
		resourceLabel.setString("Resource: " + map->hexagons[pos.y][pos.x].resource.getString());
		window->draw(resourceLabel);
	}
	entitiesLabel.setString("Entities:\nInfrastructure lvl: " + std::to_string(map->hexagons[pos.y][pos.x].infrastructureLvl) +
		"\nSupply depot lvl: " + std::to_string(map->hexagons[pos.y][pos.x].supplyDepotLvl) +
		"\nCity lvl: " + std::to_string(map->hexagons[pos.y][pos.x].townLvl) +
		"\nFactory lvl: " + map->hexagons[pos.y][pos.x].factory->showLevel());
	window->draw(entitiesLabel);
}

void Game::drawEntities(sf::Vector2i pos)
{
	GameFrame::drawEntities(pos);

	if (eMode == eUnits)
	{
		milLabel.setString("Military units: " + std::to_string(map->hexagons[selectedV.y][selectedV.x].militaryUnits.size()));
		window->draw(milLabel);
		logLabel.setString("Logistic units: " + std::to_string(map->hexagons[selectedV.y][selectedV.x].logisticUnits.size()));
		window->draw(logLabel);
		for (int i = 0; i < map->hexagons[selectedV.y][selectedV.x].militaryUnits.size(); i++)
		{
			milUnitSprite.setPosition(sf::Vector2f(entitiesBackground.getPosition().x - 150.f, i * 50.f - 50.f));
			milUnitSprite.setScale(sf::Vector2f(0.8f, 0.8f));
			window->draw(milUnitSprite);
		}
		for (int i = 0; i < map->hexagons[selectedV.y][selectedV.x].logisticUnits.size(); i++)
		{
			logUnitSprite.setPosition(sf::Vector2f(entitiesBackground.getPosition().x - 90.f, i * 58.f - 10.f));
			logUnitSprite.setScale(sf::Vector2f(0.8f, 0.8f));
			window->draw(logUnitSprite);
		}
		updateScales();
	}
}

