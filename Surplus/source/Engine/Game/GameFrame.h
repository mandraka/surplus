#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

namespace gui
{
	class Button;
	class TextBox;
	class ProgressBar;
}

enum stance : short;
class Map;
class Save;


class GameFrame
{
protected:
	float scrollSpeed;
	sf::Vector2f* globalScale;
	sf::RenderWindow *window;
	Save *save;
	Map *map;
	bool selected;
	sf::Vector2i selectedV;
	sf::SoundBuffer clickBuffer;
	sf::Sound clickSound;
	sf::SoundBuffer listBuffer;
	sf::Sound listSound;
	//statics
	sf::Font font;
	sf::Vector2f scrollOffset;
	sf::Texture baseHex;
	sf::Sprite baseSprite;
	sf::Texture nationHex;
	sf::Sprite nationSprite;
	sf::Texture townTexture;
	sf::Sprite townSprite;
	sf::Texture warehouseTexture;
	sf::Sprite warehouseSprite;
	sf::Texture factoryTexture;
	sf::Sprite factorySprite;
	sf::Texture infra1Texture;
	sf::Sprite infra1Sprite;
	sf::Texture infra2Texture;
	sf::Sprite infra2Sprite;
	sf::Texture milUnitTexture;
	sf::Sprite milUnitSprite;
	sf::Texture logUnitTexture;
	sf::Sprite logUnitSprite;
	sf::Texture ironTexture;
	sf::Sprite ironSprite;
	sf::Texture foodTexture;
	sf::Sprite foodSprite;
	sf::Texture coalTexture;
	sf::Sprite coalSprite;
	sf::Texture steelTexture;
	sf::Sprite steelSprite;
	sf::Texture woodTexture;
	sf::Sprite woodSprite;
	sf::Texture supplyTexture;
	sf::Sprite supplySprite;


	int maxScrollY, maxScrollX;
	bool scrollLeft, scrollRight, scrollUp, scrollDown;
	float hexOffset;
	stance *mainStance;

	//menu related
	bool menuOn;
	sf::RectangleShape menuShade;
	sf::RectangleShape menuBackground;
	gui::Button *continueButton;
	gui::Button *saveButton;
	gui::Button *exitButton;

	sf::RectangleShape saveBackground;
	gui::TextBox *textBox;
	gui::Button *saveToFileButton;
	gui::Button *returnButton;
	bool savingState;

	//ui related
	sf::RectangleShape uiBackground;
	sf::Text selectedCoordsLabel;
	sf::Text terrainLabel;
	sf::Text nationalityLabel;
	sf::Text resourceLabel;
	sf::Text entitiesLabel;

	gui::Button *infrastructureButton;
	gui::Button *townButton;
	gui::Button *supplyDepotButton;
	gui::Button *factoryButton;
	gui::Button *unitsButton;

	//entities interface
	bool entitiesOn;
	enum entiMode{eNull, eSupplies, eFactory, eUnits, eCity, eInfrastructure};
	entiMode eMode;
	sf::RectangleShape entitiesShade;
	sf::RectangleShape entitiesBackground;
	gui::Button *cancelButton;
	sf::Text goodsLabel;
	gui::Button *upgradeButton;
	gui::Button *milAddButton;
	sf::Text milLabel;
	gui::Button *logAddButton;
	sf::Text logLabel;
	gui::Button *factBack;

	//bars
	gui::ProgressBar *unitMove;

public:
	GameFrame(sf::RenderWindow* , stance*);
	virtual ~GameFrame();
	void loadStatics();
	void updateScales();
	virtual void initializeMenu();
	virtual void initializeUi();
	virtual void initializeEntities();
	virtual void processGameInput(sf::Event);
	void scrollHandle();
	virtual void processMenuInput(sf::Event) = 0;
	virtual void processEntitiesInput(sf::Event event) = 0;
	virtual void mainLoop() = 0;
	void drawingMainScreen();
	virtual void drawMenu();
	virtual void drawUI();
	virtual void drawHexInfo(sf::Vector2i);
	virtual void drawEntities(sf::Vector2i);
};

