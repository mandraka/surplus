#include "Menu.h"
#include <iostream>


Menu::Menu(sf::RenderWindow* window, stance* mainStance)
{
	if (!font.loadFromFile("fonts/OpenSans-Regular.ttf"))
		std::cout << "Can't load font";
	this->window = window;
	this->mainStance = mainStance;
	gameStarted = false;
}

Menu::~Menu()
{
}