#include "LoadGameMenu.h"
#include "../Gui/Button.h"
#include "../Gui/TextBox.h"
#include "../../stance.h"

LoadGameMenu::LoadGameMenu(sf::RenderWindow* window, stance* mainStance, Save* save, sf::Sound* clickSound) : Menu(window, mainStance)
{
	this->save = save;
	background.setSize(sf::Vector2f(600.f, window->getSize().y / 1.4f));
	background.setFillColor(sf::Color(100, 100, 100, 150));
	background.setPosition(sf::Vector2f(window->getSize().x / 2.f - 300.f, window->getSize().y / 8.f - 80));
	loadButton = new gui::Button("Load Game", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f * 4.f),
		sf::Vector2f(300.f, 70.f), clickSound);
	exitButton = new gui::Button("Return to Main Menu", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f * 5.f),
		sf::Vector2f(300.f, 70.f), clickSound);

	sf::Vector2i mousePos = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mousePos);
	loadButton->isHovered(worldPos);
	exitButton->isHovered(worldPos);
}

LoadGameMenu::~LoadGameMenu()
{
	delete loadButton;
	delete exitButton;
}

void LoadGameMenu::mainLoop()
{
	while (*mainStance == sLoadGameMenu)
	{
		sf::Event event;
		while (window->pollEvent(event))
			processInput(event);
		draw();
	}
}

void LoadGameMenu::processInput(sf::Event event)
{
	if (event.type == sf::Event::Closed)
	{
		*mainStance = sExit;
		return;
	}
	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (loadButton->processInput(worldPos, event))
	{
		//std::ifstream infile("new.save", std::ifstream::binary);
		//char* buffer = new char[2];
		//infile.read(buffer, 2);
		//*save.deserialize(buffer);
		//*mainStance = sGame;
	}
	else if (exitButton->processInput(worldPos, event))
		*mainStance = sMainMenu;
}

void LoadGameMenu::draw()
{
	window->clear();
	window->draw(background);
	window->draw(*loadButton);
	window->draw(*exitButton);
	window->display();
	return;
}
