#pragma once
#include "Menu.h"

namespace gui
{
	class TextBox;
	class Button;
}
class Map;

class CreateMapMenu :
	public Menu
{
private:
	Map* map;
	sf::RectangleShape background;
	gui::TextBox *textBoxX , *textBoxY;
	sf::Text sizeXLabel;
	sf::Text sizeYLabel;
	gui::Button *generateButton;
	gui::Button *returnButton;
public:
	CreateMapMenu(sf::RenderWindow*, stance*, Map*, sf::Sound*);
	~CreateMapMenu();
	void mainLoop();
	void processInput(sf::Event);
	void draw();
};

