#pragma once
#include "Menu.h"

namespace gui
{
	class Button;
}

class MainMenu :
	public Menu
{
private:
	sf::RectangleShape background;
	gui::Button* createButton;
	gui::Button* newGameButton;
	gui::Button* loadButton;
	gui::Button* optionsButton;
	gui::Button* exitButton;
public:
	MainMenu(sf::RenderWindow*, stance*, sf::Sound*);
	~MainMenu();
	void mainLoop();
	void processInput(sf::Event);
	void draw();
};

