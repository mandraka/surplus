#pragma once
#include "Menu.h"

namespace gui
{
	class Button;
}

class OptionsMenu :
	public Menu
{
private:
	sf::RectangleShape background;

	gui::Button* exitButton;
public:
	OptionsMenu(sf::RenderWindow*, stance*, sf::Sound*);
	~OptionsMenu();
	void mainLoop();
	void processInput(sf::Event);
	void draw();
};

