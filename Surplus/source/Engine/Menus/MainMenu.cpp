#include "MainMenu.h"
#include "../Gui/Button.h"
#include "../Gui/TextBox.h"
#include "../Gui/ProgressBar.h"
#include "../../stance.h"

MainMenu::MainMenu(sf::RenderWindow* window, stance* mainStance, sf::Sound* clickSound) : Menu(window, mainStance)
{
	background.setSize(sf::Vector2f(600.f, window->getSize().y / 1.4f));
	background.setFillColor(sf::Color(100, 100, 100, 150));
	background.setPosition(sf::Vector2f(window->getSize().x / 2.f - 300.f, window->getSize().y / 8.f - 80));

	createButton = new gui::Button("Create Map", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f),
		sf::Vector2f(300.f, 70.f), clickSound);
	newGameButton = new gui::Button("New Game", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f * 2.f),
		sf::Vector2f(300.f, 70.f), clickSound);
	loadButton = new gui::Button("Load game", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f * 3.f),
		sf::Vector2f(300.f, 70.f), clickSound);
	optionsButton = new gui::Button("Options", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f * 4.f),
		sf::Vector2f(300.f, 70.f), clickSound);
	exitButton = new gui::Button("Exit Game", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f * 5.f),
		sf::Vector2f(300.f, 70.f), clickSound);

	sf::Vector2i mousePos = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mousePos);
	createButton->isHovered(worldPos);
	newGameButton->isHovered(worldPos);
	loadButton->isHovered(worldPos);
	optionsButton->isHovered(worldPos);
	exitButton->isHovered(worldPos);
}

MainMenu::~MainMenu()
{
	delete createButton;
	delete newGameButton;
	delete loadButton;
	delete optionsButton;
	delete exitButton;
}

void MainMenu::mainLoop()
{
	while (*mainStance == sMainMenu)
	{
		sf::Event event;
		while (window->pollEvent(event))
			processInput(event);
		draw();
	}
}

void MainMenu::processInput(sf::Event event)
{
	if (event.type == sf::Event::Closed)
	{
		*mainStance = sExit;
		return;
	}
	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (createButton->processInput(worldPos, event))
		*mainStance = sCreateMapMenu;
	else if (newGameButton->processInput(worldPos, event))
		*mainStance = sNewGameMenu;
	else if (loadButton->processInput(worldPos, event))
		*mainStance = sLoadGameMenu;
	else if (optionsButton->processInput(worldPos, event))
		*mainStance = sOptions;
	else if (exitButton->processInput(worldPos, event))
	{
		window->close();
		*mainStance = sExit;
	}
}

void MainMenu::draw()
{
	window->clear();

	window->draw(background);
	window->draw(*createButton);
	window->draw(*newGameButton);
	window->draw(*loadButton);
	window->draw(*optionsButton);
	window->draw(*exitButton);

	window->display();

	return;
}
