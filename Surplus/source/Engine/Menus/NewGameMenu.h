#pragma once
#include "Menu.h"
#include <vector>

class Map;
class Save;
enum nationality : short;

namespace gui
{
	class Button;
}

class NewGameMenu :
	public Menu
{
private:
	Map* map;
	Save* save;
	nationality* player;
	sf::RectangleShape background;
	std::vector<std::string> saveNames;
	int selectedIndex;
	gui::Button* saves[20];
	gui::Button* newGameButton;
	gui::Button* exitButton;
public:
	NewGameMenu(sf::RenderWindow*, stance*, Map*, Save*, nationality*, sf::Sound*);
	~NewGameMenu();
	void mainLoop();
	void processInput(sf::Event);
	void draw();
};

