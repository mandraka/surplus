#include "CreateMapMenu.h"
#include "../Gui/Button.h"
#include "../Gui/TextBox.h"
#include "../../stance.h"
#include "../Map.h"

CreateMapMenu::CreateMapMenu(sf::RenderWindow* window, stance* mainStance, Map* map, sf::Sound* clickSound) : Menu(window, mainStance)
{
	this->map = map;

	background.setSize(sf::Vector2f(800.f, window->getSize().y / 1.85f));
	background.setFillColor(sf::Color(100, 100, 100, 150));
	background.setPosition(sf::Vector2f(window->getSize().x / 2.f - 400.f, window->getSize().y / 7.5f));

	textBoxX = new gui::TextBox(sf::Vector2f(window->getSize().x / 2.f - 100.f, window->getSize().y / 4.35f),
		sf::Vector2f(200.f, 60.f));
	textBoxX->setNumbersOnly(true);
	textBoxY = new gui::TextBox(sf::Vector2f(window->getSize().x / 2.f - 100.f, window->getSize().y / 4.35f + (textBoxX->getSize().y + 20)),
		sf::Vector2f(200.f, 60.f));
	textBoxY->setNumbersOnly(true);
	textBoxX->other = textBoxY;
	textBoxX->maxValue = 500;
	textBoxY->other = textBoxX;
	textBoxY->maxValue = 500;
	generateButton = new gui::Button("Generate Map", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 7.f * 3.f),
		sf::Vector2f(300.f, 80.f), clickSound);
	returnButton = new gui::Button("Return to Main Menu", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 7.f * 4.f),
		sf::Vector2f(300.f, 80.f), clickSound);

	sizeXLabel.setString("Set width of the new map");
	sizeXLabel.setFont(font);
	sizeXLabel.setCharacterSize(20);
	sizeXLabel.setFillColor(sf::Color::White);
	sizeXLabel.setPosition(textBoxX->getPosition().x - sizeXLabel.getString().getSize()*11.f,
		textBoxX->getPosition().y + textBoxX->getSize().y / 2 - sizeXLabel.getCharacterSize() / 2 - 5);
	sizeYLabel.setString("Set height of the new map");
	sizeYLabel.setFont(font);
	sizeYLabel.setCharacterSize(20);
	sizeYLabel.setFillColor(sf::Color::White);
	sizeYLabel.setPosition(textBoxY->getPosition().x - sizeYLabel.getString().getSize()*11.f,
		textBoxY->getPosition().y + textBoxY->getSize().y / 2 - sizeYLabel.getCharacterSize() / 2 - 5);

	sf::Vector2i mousePos = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mousePos);
	generateButton->isHovered(worldPos);
	returnButton->isHovered(worldPos);
}

CreateMapMenu::~CreateMapMenu()
{
	delete generateButton;
	delete returnButton;
	delete textBoxX;
	delete textBoxY;
}

void CreateMapMenu::mainLoop()
{
	while (*mainStance == sCreateMapMenu)
	{
		sf::Event event;
		while (window->pollEvent(event))
			processInput(event);
		draw();
	}
}

void CreateMapMenu::processInput(sf::Event event)
{
	if (event.type == sf::Event::Closed)
	{
		*mainStance = sExit;
		return;
	}

	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (returnButton->processInput(worldPos, event))
		*mainStance = sMainMenu;
	else if (generateButton->processInput(worldPos, event) &&
		textBoxX->getInput() != "" && textBoxY->getInput() != "")
	{
		*map = *new Map(sf::Vector2i(textBoxX->getIntInput(), textBoxY->getIntInput()));
		*mainStance = sMapEdit;
	}

	textBoxX->processInput(worldPos, event);
	textBoxY->processInput(worldPos, event);
}

void CreateMapMenu::draw()
{
	window->clear();

	window->draw(background);
	window->draw(sizeXLabel);
	window->draw(sizeYLabel);
	window->draw(*generateButton);
	window->draw(*returnButton);
	window->draw(*textBoxX);
	window->draw(*textBoxY);

	window->display();

	return;
}

