#pragma once
#include "Menu.h"
//#include "../Save.h"
namespace gui
{
	class Button;
}
class Save;

class LoadGameMenu :
	public Menu
{
private:
	Save* save;
	sf::RectangleShape background;
	gui::Button* loadButton;
	gui::Button* exitButton;
public:
	LoadGameMenu(sf::RenderWindow*, stance*, Save*, sf::Sound*);
	~LoadGameMenu();
	void mainLoop();
	void processInput(sf::Event);
	void draw();
};

