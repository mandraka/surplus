#pragma once
#include <SFML/Graphics.hpp>

namespace sf
{
	class Sound;
	class SoundBuffer;
}

enum stance : short;

class Menu
{
protected:
	sf::Font font;
	stance* mainStance;
public:
	bool gameStarted;
	sf::RenderWindow *window;

	Menu(sf::RenderWindow*, stance*);
	virtual ~Menu();
	virtual void mainLoop() = 0;
	virtual void draw() = 0;
	virtual void processInput(sf::Event) = 0;
};

