#include "OptionsMenu.h"
#include "../Gui/Button.h"
#include "../../stance.h"

OptionsMenu::OptionsMenu(sf::RenderWindow* window, stance* mainStance, sf::Sound* clickSound) : Menu(window, mainStance)
{
	background.setSize(sf::Vector2f(600.f, window->getSize().y / 1.4f));
	background.setFillColor(sf::Color(100, 100, 100, 150));
	background.setPosition(sf::Vector2f(window->getSize().x / 2.f - 300.f, window->getSize().y / 8.f - 80));
	exitButton = new gui::Button("Return to Main Menu", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f * 5.f),
		sf::Vector2f(300.f, 70.f), clickSound);

	sf::Vector2i mousePos = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mousePos);
	exitButton->isHovered(worldPos);
}

OptionsMenu::~OptionsMenu()
{
	delete exitButton;
}

void OptionsMenu::mainLoop()
{
	while (*mainStance == sOptions)
	{
		sf::Event event;
		while (window->pollEvent(event))
			processInput(event);
		draw();
	}
}

void OptionsMenu::processInput(sf::Event event)
{
	if (event.type == sf::Event::Closed)
	{
		*mainStance = sExit;
		return;
	}

	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (exitButton->processInput(worldPos, event))
		*mainStance = sMainMenu;
}

void OptionsMenu::draw()
{
	window->clear();
	window->draw(background);
	window->draw(*exitButton);
	window->display();

	return;
}

