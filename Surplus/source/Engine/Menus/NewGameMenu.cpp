#include "NewGameMenu.h"
#include <fstream>
#include <windows.h>
#include <vector>
#include "../Gui/Button.h"
#include "../Gui/TextBox.h"
#include "../../Engine/Save.h"
#include "../../stance.h"
#include "../../Fields/nationality.h"


NewGameMenu::NewGameMenu(sf::RenderWindow* window, stance* mainStance, Map* map, Save* save, nationality* player, sf::Sound* clickSound) : Menu(window, mainStance)
{
	this->map = map;
	this->save = save;
	*player = red;
	this->player = player;
	selectedIndex = -1;

	background.setSize(sf::Vector2f(600.f, window->getSize().y / 1.4f));
	background.setFillColor(sf::Color(100, 100, 100, 150));
	background.setPosition(sf::Vector2f(window->getSize().x / 2.f - 300.f, window->getSize().y / 8.f - 80));
	newGameButton = new gui::Button("New Game", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f * 4.f),
		sf::Vector2f(300.f, 70.f), clickSound);
	exitButton = new gui::Button("Return to Main Menu", sf::Vector2f(window->getSize().x / 2.f - 150.f, window->getSize().y / 8.f * 5.f),
		sf::Vector2f(300.f, 70.f), clickSound);

	WIN32_FIND_DATA data;
	HANDLE hFind = FindFirstFile(".\\saves\\*.map", &data);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			saveNames.push_back(data.cFileName);
		} while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}
	for (int i = 0; i < saveNames.size(); i++)
		saves[i] = new gui::Button(std::string(saveNames[i]).c_str(),
			sf::Vector2f(float(window->getSize().x) / 2.f - 150.f, 100.f * float(i) + 100.f), sf::Vector2f(150, 70), clickSound);
	sf::Vector2i mousePos = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mousePos);
	newGameButton->isHovered(worldPos);
	exitButton->isHovered(worldPos);
}

NewGameMenu::~NewGameMenu()
{
	delete newGameButton;
	delete exitButton;
	//delete[] saves;
	for (int i = 0; i < saveNames.size(); i++)
		delete saves[i];
}

void NewGameMenu::mainLoop()
{
	while (*mainStance == sNewGameMenu)
	{
		sf::Event event;
		while (window->pollEvent(event))
			processInput(event);
		draw();
	}
}

void NewGameMenu::processInput(sf::Event event)
{
	if (event.type == sf::Event::Closed)
	{
		*mainStance = sExit;
		return;
	}
	sf::Vector2i mouseClick = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(mouseClick);
	if (newGameButton->processInput(worldPos, event))
	{
		if (selectedIndex > -1)
		{
			std::ifstream infile(".\\saves\\" + saves[selectedIndex]->getString(), std::ifstream::binary);
			char* buffer = new char[536870912]; //536 mb
			infile.read(buffer, 536870912);
			map = new Map();
			map->player = player;
			map->deserialize(buffer, map);
			*save = Save(map);
			*mainStance = sGame;
			infile.clear();
			infile.close();
			delete[] buffer;
		}
	}
	else if (exitButton->processInput(worldPos, event))
		*mainStance = sMainMenu;

	for (int i = 0; i < saveNames.size(); i++)
	{
		if (saves[i]->processInput(worldPos, event))
			selectedIndex = i;
	}
}

void NewGameMenu::draw()
{
	window->clear();

	window->draw(background);
	window->draw(*newGameButton);
	window->draw(*exitButton);
	for (int i = 0; i < saveNames.size(); i++)
		window->draw(*saves[i]);
	window->display();

	return;
}
