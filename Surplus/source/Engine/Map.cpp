#include "Map.h"
//#include <string>
#include "../Units/MilitaryUnit.h"
#include "../Units/LogisticUnit.h"
#include "../Fields/Hexagon.h"
#include "../Fields/nationality.h"

Map::Map()
{
	empty = true;
	hexagons = nullptr;
}

Map::Map(sf::Vector2i size)
{
	this->size = size;
	lastSelected.y = -1;
	empty = false;

	this->hexagons = new Hexagon*[size.y];
	for (int i = 0; i < size.y; i++)
		this->hexagons[i] = new Hexagon[size.x];

	for (int i = 0; i < size.y; i++)
		for (int j = 0; j < size.x; j++)
		{
			this->hexagons[i][j].setPos(i, j);
			this->hexagons[i][j].wasAStarred = false;
		}
}

Map::~Map()
{
	if (hexagons != nullptr)
	{
		for (int i = 0; i < size.y; i++)
			delete[] hexagons[i];
		delete[] hexagons;
	}
}

Map& Map::operator=(const Map & other)
{
	if (this == &other)
		return *this;
	size = other.size;
	lastSelected = other.lastSelected;
	hexagons = other.hexagons;
	empty = other.empty;
	return *this;
}

sf::Vector2i Map::getSize()
{
	return size;
}

bool Map::isEmpty()
{
	return empty;
}

void Map::step()
{
	for (int i = 0; i < size.y; i++)
		for (int j = 0; j < size.x; j++)
			this->hexagons[i][j].step();
}
union U {
	short s;  // or use int16_t to be more specific
	//   vs.
	struct Byte {
		char c1, c2;  // or use int8_t to be more specific
	}
	byte;
};
char* Map::serialize()
{
	char* output = new char[4 + size.x * size.y * 14]; //////////////////////////////////////////////////
	// writting size of map size variables
	U u;
	u.s = size.x;
	output[0] = u.byte.c1;
	output[1] = u.byte.c2;
	u.s = size.y;
	output[2] = u.byte.c1;
	output[3] = u.byte.c2;

	//writing all hexagons //////////////////
	int independentIterator = 4;
	for (int i = 0; i < size.y; i++)
		for (int j = 0; j < size.x; j++)
		{
			output[independentIterator] = 0;
			if (hexagons[i][j].getNationality() > 0)
				output[independentIterator] += hexagons[i][j].getNationality() << 4;
			if (hexagons[i][j].getTerrain() > 0)
				output[independentIterator] += hexagons[i][j].getTerrain();
			independentIterator++;
			output[independentIterator] = 0;
			output[independentIterator] += hexagons[i][j].infrastructureLvl << 4;
			output[independentIterator] += hexagons[i][j].townLvl;
			independentIterator++;
			output[independentIterator] = 0;
			output[independentIterator] += hexagons[i][j].supplyDepotLvl;
			independentIterator++;
			output[independentIterator] = 0;
			output[independentIterator] += hexagons[i][j].resource.getType();
			independentIterator++;
			output[independentIterator] = 0;
			output[independentIterator] += int(hexagons[i][j].militaryUnits.size());
			independentIterator++;
			output[independentIterator] = 0;
			output[independentIterator] += int(hexagons[i][j].logisticUnits.size());
			independentIterator++;
			//resources
			u.s = hexagons[i][j].getResource(iron);
			output[independentIterator] = u.byte.c1;
			independentIterator++;
			output[independentIterator] = u.byte.c2;
			independentIterator++;

			u.s = hexagons[i][j].getResource(food);
			output[independentIterator] = u.byte.c1;
			independentIterator++;
			output[independentIterator] = u.byte.c2;
			independentIterator++;

			u.s = hexagons[i][j].getResource(coal);
			output[independentIterator] = u.byte.c1;
			independentIterator++;
			output[independentIterator] = u.byte.c2;
			independentIterator++;

			u.s = hexagons[i][j].getResource(wood);
			output[independentIterator] = u.byte.c1;
			independentIterator++;
			output[independentIterator] = u.byte.c2;
			independentIterator++;
		}
	return output;
}

void Map::deserialize(char* data, Map* map)
{
	//read map dimensions variables
	U u;
	u.byte.c1 = data[0];
	u.byte.c2 = data[1];
	short x = u.s;
	u.byte.c1 = data[2];
	u.byte.c2 = data[3];
	sf::Vector2i saveSize(x, u.s);
	//initialize map
	*map = *new Map(saveSize);

	//read all hexagons
	int independentIterator = 4;
	for (int i = 0; i < size.y; i++)
		for (int j = 0; j < size.x; j++)
		{
			hexagons[i][j].setTerrain((terrainType)(data[independentIterator] & 15));
			hexagons[i][j].setNationality((nationality)(data[independentIterator] >> 4));
			independentIterator++;
			hexagons[i][j].townLvl = data[independentIterator] & 15;
			hexagons[i][j].infrastructureLvl = (data[independentIterator] >> 4);
			independentIterator++;
			hexagons[i][j].supplyDepotLvl = data[independentIterator];
			independentIterator++;
			hexagons[i][j].resource.setType((TypeOfResource)data[independentIterator]);
			independentIterator++;
			for (int k = 0; k < data[independentIterator]; k++)
				hexagons[i][j].militaryUnits.push_back(MilitaryUnit(this, sf::Vector2i(j, i), k));
			independentIterator++;
			for (int k = 0; k < data[independentIterator]; k++)
				hexagons[i][j].logisticUnits.push_back(LogisticUnit(this, sf::Vector2i(j, i), k));
			independentIterator++;
			//resources
			u.byte.c1 = data[independentIterator];
			independentIterator++;
			u.byte.c2 = data[independentIterator];
			hexagons[i][j].changeResource(iron, u.s);

			independentIterator++;
			u.byte.c1 = data[independentIterator];
			independentIterator++;
			u.byte.c2 = data[independentIterator];
			hexagons[i][j].changeResource(food, u.s);

			independentIterator++;
			u.byte.c1 = data[independentIterator];
			independentIterator++;
			u.byte.c2 = data[independentIterator];
			hexagons[i][j].changeResource(coal, u.s);

			independentIterator++;
			u.byte.c1 = data[independentIterator];
			independentIterator++;
			u.byte.c2 = data[independentIterator];
			hexagons[i][j].changeResource(wood, u.s);
			independentIterator++;
		}
}

void Map::makeSurroundingsVisible(sf::Vector2i pos, int depth)
{
	if (pos.x < 0 || pos.x >= getSize().x || pos.y >= getSize().y || pos.y < 0)
		return;
	hexagons[pos.y][pos.x].visible = true;
	if (depth > 0)
	{
		makeSurroundingsVisible(sf::Vector2i(pos.x, pos.y - 1), depth - 1);
		makeSurroundingsVisible(sf::Vector2i(pos.x, pos.y + 1), depth - 1);
		makeSurroundingsVisible(sf::Vector2i(pos.x + 1, pos.y), depth - 1);
		makeSurroundingsVisible(sf::Vector2i(pos.x - 1, pos.y), depth - 1);
		if (pos.x % 2 == 1)
		{
			makeSurroundingsVisible(sf::Vector2i(pos.x + 1, pos.y + 1), depth - 1);
			makeSurroundingsVisible(sf::Vector2i(pos.x - 1, pos.y + 1), depth - 1);
		}
		else if (pos.x % 2 == 0)
		{
			makeSurroundingsVisible(sf::Vector2i(pos.x + 1, pos.y - 1), depth - 1);
			makeSurroundingsVisible(sf::Vector2i(pos.x - 1, pos.y - 1), depth - 1);
		}
	}
	else return;
}