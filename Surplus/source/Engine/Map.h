#pragma once
#include <SFML/System/Vector2.hpp>

class Hexagon;
enum nationality : short;

class Map
{
private:
	sf::Vector2i size;
	bool empty;
public:
	sf::Vector2i lastSelected;
	nationality *player;
	Hexagon** hexagons;

	Map();
	Map(sf::Vector2i);
	~Map();
	Map& operator=(const Map &);
	sf::Vector2i getSize();
	bool isEmpty();
	void step();
	char* serialize();
	void deserialize(char*, Map*);
	void makeSurroundingsVisible(sf::Vector2i pos, int depth);
};