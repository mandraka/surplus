#include "Unit.h"
#include "../Engine/Map.h"
#include "../Fields/Hexagon.h"
#include <iostream>

Unit::Unit()
{
	moveProgress = -1;
}

Unit::Unit(Map *map, sf::Vector2i mapPos, int index)
{
	this->map = map;
	this->mapPos = mapPos;
	this->index = index;
	speed = 10;
	moveProgress = -1;
}

Unit::~Unit()
{
}

void Unit::step()
{
	std::cout << moveProgress << std::endl;
	if (moveProgress > -1)
	{
		if (map->hexagons[road.back().y][road.back().x].getNationality() == map->hexagons[mapPos.y][mapPos.x].getNationality())
			//if passing through own territory
			moveProgress += speed * map->hexagons[mapPos.y][mapPos.x].speedMultiplier;
		else //if passing through enemy territory
		{
			if (map->hexagons[road.back().y][road.back().x].militaryUnits.size() > 0) //if enemy appears on horizon 
			{
				if (typeid(*this) == typeid(MilitaryUnit)) //if military
				{
					std::cout << "Attacking manpower: " << map->hexagons[mapPos.y][mapPos.x].militaryUnits[index].manpower.getAmmount() <<
						" attacked manpower: " << map->hexagons[road.back().y][road.back().x].militaryUnits[0].manpower.getAmmount() << std::endl;
					map->hexagons[mapPos.y][mapPos.x].militaryUnits[index].attack(sf::Vector2i(road.back().x, road.back().y));
					return;
				}
				else if (typeid(*this) == typeid(LogisticUnit)) //if logistic
				{
					moveProgress = -1;
					road.clear();
					return;
				}
			}
			moveProgress += speed * map->hexagons[mapPos.y][mapPos.x].speedMultiplier * 0.5f;
		}
	}
}
