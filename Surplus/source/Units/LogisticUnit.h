#pragma once
#include "Unit.h"

class LogisticUnit :
	public Unit
{
private:
	int maxCargo;
	int cargo;
	Resource cargoType;
	
public:
	LogisticUnit();
	LogisticUnit(const LogisticUnit &);
	LogisticUnit(Map *, sf::Vector2i, int);
	~LogisticUnit();
	LogisticUnit & operator = (LogisticUnit &&);
	LogisticUnit & operator = (LogisticUnit &);
	void step();
};

