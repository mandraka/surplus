#include "MilitaryUnit.h"
#include "../Engine/Map.h"
#include "../Fields/Hexagon.h"
#include <random>


MilitaryUnit::MilitaryUnit() : Unit()
{
	combatPerformance = 10;
	maxManpower = 1000;
	this->manpower.setAmmount(maxManpower);
}

MilitaryUnit::MilitaryUnit(const MilitaryUnit& other)
{
	combatPerformance = other.combatPerformance;
	maxManpower = other.maxManpower;
	manpower = other.manpower;
	road = other.road;
	isEmpty = other.isEmpty;
	moveProgress = other.moveProgress;
	map = other.map;
	mapPos = other.mapPos;
	speed = other.speed;
	index = other.index;
}

MilitaryUnit::MilitaryUnit(Map *map, sf::Vector2i mapPos, int index) : Unit(map, mapPos, index)
{
	combatPerformance = 10;
	maxManpower = 1000;
	this->manpower.setAmmount(maxManpower);
}

MilitaryUnit::~MilitaryUnit()
{
}

MilitaryUnit & MilitaryUnit::operator=(MilitaryUnit && other)
{
	if (&other == this)
		return *this;
	combatPerformance = other.combatPerformance;
	maxManpower = other.maxManpower;
	manpower.setAmmount(other.manpower.getAmmount());
	road = other.road;
	isEmpty = other.isEmpty;
	moveProgress = other.moveProgress;
	map = other.map;
	mapPos = other.mapPos;
	speed = other.speed;
	index = other.index;
	return *this;
}

MilitaryUnit & MilitaryUnit::operator=(MilitaryUnit & other)
{
	if (&other == this)
		return *this;
	combatPerformance = other.combatPerformance;
	maxManpower = other.maxManpower;
	manpower.setAmmount(other.manpower.getAmmount());
	road = other.road;
	isEmpty = other.isEmpty;
	moveProgress = other.moveProgress;
	map = other.map;
	mapPos = other.mapPos;
	speed = other.speed;
	index = other.index;
	isEmpty = other.isEmpty;
	return *this;
}

void MilitaryUnit::step()
{
	if (moveProgress > 100)
	{
		int currentPosX = mapPos.x;
		int currentPosY = mapPos.y;
		int startingIndex = index;
		sf::Vector2i movement;
		movement = road.back();
		road.pop_back();
		if (!road.empty())
			moveProgress = 0;
		else
			moveProgress = -1;

		if (map->hexagons[movement.y][movement.x].getNationality() == map->hexagons[mapPos.y][mapPos.x].getNationality())
		{
			index = int(map->hexagons[movement.y][movement.x].militaryUnits.size());
			mapPos = movement;
			map->hexagons[movement.y][movement.x].militaryUnits.push_back(*this);
		}
		else
		{
			map->hexagons[movement.y][movement.x].setNationality(map->hexagons[mapPos.y][mapPos.x].getNationality());
			if (map->hexagons[mapPos.y][mapPos.x].getNationality() == *map->player) // the player is advancing
				map->makeSurroundingsVisible(sf::Vector2i(movement.x, movement.y), 2);
			index = int(map->hexagons[movement.y][movement.x].militaryUnits.size());
			mapPos = movement;
			map->hexagons[movement.y][movement.x].militaryUnits.push_back(*this);
		}

		map->hexagons[currentPosY][currentPosX].militaryUnits.erase(map->hexagons[currentPosY][currentPosX].militaryUnits.begin() + startingIndex); //remove from current hex
		if (map->hexagons[currentPosY][currentPosX].militaryUnits.size() > startingIndex) // index update
			for (int i = startingIndex; i < map->hexagons[currentPosY][currentPosX].militaryUnits.size(); i++)
				map->hexagons[currentPosY][currentPosX].militaryUnits[i].index--;
		map->hexagons[currentPosY][currentPosX].selectedMilUnit = -1;
	}
	else
		Unit::step();
}

void MilitaryUnit::attack(sf::Vector2i target)
{
	float advantage = (combatPerformance * (float(rand()% 4 / 10.f) + 1.f)) -
		(map->hexagons[target.y][target.x].militaryUnits[0].combatPerformance * (float(rand() % 4 / 10.f) + 1.f));
	map->hexagons[target.y][target.x].militaryUnits[0].manpower.changeAmmount(-20 - advantage * (rand() % 2 / 10 + 1));
	this->manpower.changeAmmount(-20 + advantage * (rand() % 2 / 10 + 1));
	map->hexagons[target.y][target.x].militaryUnits[0].road.clear();
	map->hexagons[target.y][target.x].militaryUnits[0].checkIfAlive();
	checkIfAlive();
}

void MilitaryUnit::checkIfAlive()
{
	if (this->manpower.getAmmount() < 0)
	{
		map->hexagons[mapPos.y][mapPos.x].militaryUnits.erase(map->hexagons[mapPos.y][mapPos.x].militaryUnits.begin() + index); //remove from current hex
		if (map->hexagons[mapPos.y][mapPos.x].militaryUnits.size() > index) // index update
			for (int i = index; i < map->hexagons[mapPos.y][mapPos.x].militaryUnits.size(); i++)
				map->hexagons[mapPos.y][mapPos.x].militaryUnits[i].index--;
		map->hexagons[mapPos.y][mapPos.x].selectedMilUnit = -1;
		this->~MilitaryUnit();
	}
	
}


