#pragma once
#include <vector>
#include <SFML/Graphics/Drawable.hpp>
#include "../Fields/Resource.h"

class Map;

class Unit
{
private:

protected:
	Map *map;
	sf::Vector2i mapPos;
public:
	bool isEmpty;
	int index;
	std::string name;
	float moveProgress;
	std::vector<sf::Vector2i> road;
	int speed;
	Unit();
	Unit(Map *, sf::Vector2i, int);
	virtual ~Unit();
	void aStar(sf::Vector2i);
	virtual void step();
};

