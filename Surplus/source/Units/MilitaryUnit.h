#pragma once
#include "Unit.h"


class MilitaryUnit :
	public Unit
{
private:
	int maxManpower;
	int maxOrganisation;
	int organisation;
	Resource supply;
public:
	Resource manpower;
	float combatPerformance;

	MilitaryUnit();
	MilitaryUnit(const MilitaryUnit &);
	MilitaryUnit(Map *, sf::Vector2i, int);
	~MilitaryUnit();
	MilitaryUnit & operator = (MilitaryUnit &&);
	MilitaryUnit & operator = (MilitaryUnit &);
	void step();
	void attack(sf::Vector2i);
	void checkIfAlive();

};

