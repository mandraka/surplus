#include "LogisticUnit.h"
#include "../Engine/Map.h"
#include "../Fields/Hexagon.h"

LogisticUnit::LogisticUnit() : Unit()
{
}

LogisticUnit::LogisticUnit(const LogisticUnit& other)
{
	//	if (this == &other)
		//	return;
	road = other.road;
	isEmpty = other.isEmpty;
	moveProgress = other.moveProgress;
	map = other.map;
	mapPos = other.mapPos;
	speed = other.speed;
	index = other.index;
}

LogisticUnit::LogisticUnit(Map *map, sf::Vector2i mapPos, int index) : Unit(map, mapPos, index)
{
}

LogisticUnit::~LogisticUnit()
{
}

LogisticUnit & LogisticUnit::operator=(LogisticUnit && other)
{
	if (&other == this)
		return *this;
	road = other.road;
	isEmpty = other.isEmpty;
	return *this;
}
LogisticUnit & LogisticUnit::operator=(LogisticUnit & other)
{
	if (&other == this)
		return *this;
	road = other.road;
	isEmpty = other.isEmpty;
	return *this;
}
void LogisticUnit::step()
{
	if (moveProgress > 100)
	{
		int currentPosX = mapPos.x;
		int currentPosY = mapPos.y;
		int startingIndex = index;
		sf::Vector2i movement;
		movement = road.back();
		road.pop_back();
		if (!road.empty())
			moveProgress = 0;
		else
			moveProgress = -1;

		if (map->hexagons[movement.y][movement.x].getNationality() == map->hexagons[mapPos.y][mapPos.x].getNationality())
		{
			index = int(map->hexagons[movement.y][movement.x].logisticUnits.size());
			mapPos = movement;
			map->hexagons[movement.y][movement.x].logisticUnits.push_back(*this);
		}

		map->hexagons[currentPosY][currentPosX].logisticUnits.erase(map->hexagons[currentPosY][currentPosX].logisticUnits.begin() + startingIndex); //remove from current hex
		if (map->hexagons[currentPosY][currentPosX].logisticUnits.size() > startingIndex) // index update
			for (int i = startingIndex; i < map->hexagons[currentPosY][currentPosX].logisticUnits.size(); i++)
				map->hexagons[currentPosY][currentPosX].logisticUnits[i].index--;
		map->hexagons[currentPosY][currentPosX].selectedLogUnit = -1;
	}
	else
		Unit::step();
}
