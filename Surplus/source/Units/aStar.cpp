#include <typeinfo>
#include "Unit.h"
#include "../Engine/Map.h"
#include "../Fields/Hexagon.h"

void Unit::aStar(sf::Vector2i to)
{
	if (to == this->mapPos)
	{
		moveProgress = -1;
		road.empty();
		return;
	}

	this->moveProgress = 0;
	std::vector<float> lengths;
	std::vector<std::vector<sf::Vector2i>> roads;

	std::vector<sf::Vector2i> rS;
	rS.push_back(sf::Vector2i(mapPos.x, mapPos.y));
	map->hexagons[mapPos.y][mapPos.x].wasAStarred = true;
	float lS = map->hexagons[mapPos.y][mapPos.x].speedMultiplier;
	lengths.push_back(lS);
	roads.push_back(rS);

	while (true)
	{
		//check current paths
		int shortestInd = 0;
		for (int i = 1; i < lengths.size(); i++)
		{
			if (lengths[shortestInd] > lengths[i])
				shortestInd = i;
		}
		sf::Vector2i last = roads[shortestInd][roads[shortestInd].size() - 1];

		/////////////////////////////////////////////////////expansion
		// common points there
		if (last.y > 0)
			if (map->hexagons[last.y - 1][last.x].wasAStarred == false)
			{
				bool foreignMovement = false;
				if (map->hexagons[last.y - 1][last.x].getNationality() != map->hexagons[last.y][last.x].getNationality())
					foreignMovement = true;
				if (foreignMovement && typeid(*this).name() == "LogisticUnit")
					return;
				map->hexagons[last.y - 1][last.x].wasAStarred = true;
				if (sf::Vector2i(last.x, last.y - 1) == to)
				{
					road.clear();
					roads[shortestInd].push_back(sf::Vector2i(last.x, last.y - 1));
					for (int i = int(roads[shortestInd].size()) - 1; i > 0; i--) // add road to main unit road
						road.push_back(roads[shortestInd][i]);
					break;
				}
				std::vector<sf::Vector2i> r = roads[shortestInd];
				r.push_back(sf::Vector2i(last.x, last.y - 1));
				roads.push_back(r);
				float l;
				if (!foreignMovement)
					l = lengths[shortestInd] + 1 / map->hexagons[last.y - 1][last.x].speedMultiplier;
				else
					l = lengths[shortestInd] + 1 / (map->hexagons[last.y - 1][last.x].speedMultiplier* 0.5f);
				lengths.push_back(l);
			}
		if (last.y < map->getSize().y - 1)
		{
			if (map->hexagons[last.y + 1][last.x].wasAStarred == false)
			{
				bool foreignMovement = false;
				if (map->hexagons[last.y + 1][last.x].getNationality() != map->hexagons[last.y][last.x].getNationality())
					foreignMovement = true;
				if (foreignMovement && typeid(*this).name() == "LogisticUnit")
					return;
				map->hexagons[last.y + 1][last.x].wasAStarred = true;
				if (sf::Vector2i(last.x, last.y + 1) == to)
				{
					road.clear();
					roads[shortestInd].push_back(sf::Vector2i(last.x, last.y + 1));
					for (int i = int(roads[shortestInd].size()) - 1; i > 0; i--) // add road to main unit road
						road.push_back(roads[shortestInd][i]);
					break;
				}
				std::vector<sf::Vector2i> r = roads[shortestInd];
				r.push_back(sf::Vector2i(last.x, last.y + 1));
				roads.push_back(r);
				float l;
				if (!foreignMovement)
					l = lengths[shortestInd] + 1 / map->hexagons[last.y + 1][last.x].speedMultiplier;
				else
					l = lengths[shortestInd] + 1 / (map->hexagons[last.y + 1][last.x].speedMultiplier * 0.5f);
				lengths.push_back(l);
			}
		}
		if (last.x < map->getSize().x - 1)
		{
			if (map->hexagons[last.y][last.x + 1].wasAStarred == false)
			{
				bool foreignMovement = false;
				if (map->hexagons[last.y][last.x + 1].getNationality() != map->hexagons[last.y][last.x].getNationality())
					foreignMovement = true;
				if (foreignMovement && typeid(*this).name() == "LogisticUnit")
					return;
				map->hexagons[last.y][last.x + 1].wasAStarred = true;
				if (sf::Vector2i(last.x + 1, last.y) == to)
				{
					road.clear();
					roads[shortestInd].push_back(sf::Vector2i(last.x + 1, last.y));
					for (int i = int(roads[shortestInd].size()) - 1; i > 0; i--) // add road to main unit road
						road.push_back(roads[shortestInd][i]);
					break;
				}
				std::vector<sf::Vector2i> r = roads[shortestInd];
				r.push_back(sf::Vector2i(last.x + 1, last.y));
				roads.push_back(r);
				float l;
				if (!foreignMovement)
					l = lengths[shortestInd] + 1 / map->hexagons[last.y][last.x + 1].speedMultiplier;
				else
					l = lengths[shortestInd] + 1 / (map->hexagons[last.y][last.x + 1].speedMultiplier * 0.5f);
				lengths.push_back(l);
			}
		}

		if (last.x > 0)
		{
			if (map->hexagons[last.y][last.x - 1].wasAStarred == false)
			{
				bool foreignMovement = false;
				if (map->hexagons[last.y][last.x - 1].getNationality() != map->hexagons[last.y][last.x].getNationality())
					foreignMovement = true;
				if (foreignMovement && typeid(*this).name() == "LogisticUnit")
					return;
				map->hexagons[last.y][last.x - 1].wasAStarred = true;
				if (sf::Vector2i(last.x - 1, last.y) == to)
				{
					road.clear();
					roads[shortestInd].push_back(sf::Vector2i(last.x - 1, last.y));
					for (int i = int(roads[shortestInd].size()) - 1; i > 0; i--) // add road to main unit road
						road.push_back(roads[shortestInd][i]);
					break;
				}
				std::vector<sf::Vector2i> r = roads[shortestInd];
				r.push_back(sf::Vector2i(last.x - 1, last.y));
				roads.push_back(r);
				float l;
				if (!foreignMovement)
					l = lengths[shortestInd] + 1 / map->hexagons[last.y][last.x - 1].speedMultiplier;
				else
					l = lengths[shortestInd] + 1 / (map->hexagons[last.y][last.x - 1].speedMultiplier * 0.5f);
				lengths.push_back(l);
			}
		}
		if (last.x % 2 == 1)
		{
			if (last.y < map->getSize().y - 1)
			{
				if (last.x < map->getSize().x - 1)
				{
					if (map->hexagons[last.y + 1][last.x + 1].wasAStarred == false)
					{
						bool foreignMovement = false;
						if (map->hexagons[last.y + 1][last.x + 1].getNationality() != map->hexagons[last.y][last.x].getNationality())
							foreignMovement = true;
						if (foreignMovement && typeid(*this).name() == "LogisticUnit")
							return;
						map->hexagons[last.y + 1][last.x + 1].wasAStarred = true;
						if (sf::Vector2i(last.x + 1, last.y + 1) == to)
						{
							road.clear();
							roads[shortestInd].push_back(sf::Vector2i(last.x + 1, last.y + 1));
							for (int i = int(roads[shortestInd].size()) - 1; i > 0; i--) // add road to main unit road
								road.push_back(roads[shortestInd][i]);
							break;
						}
						std::vector<sf::Vector2i> r = roads[shortestInd];
						r.push_back(sf::Vector2i(last.x + 1, last.y + 1));
						roads.push_back(r);
						float l;
						if (!foreignMovement)
							l = lengths[shortestInd] + 1 / map->hexagons[last.y + 1][last.x + 1].speedMultiplier;
						else
							l = lengths[shortestInd] + 1 / (map->hexagons[last.y + 1][last.x + 1].speedMultiplier* 0.5f);
						lengths.push_back(l);
					}
				}
				if (last.x > 0)
				{
					if (map->hexagons[last.y + 1][last.x - 1].wasAStarred == false)
					{
						bool foreignMovement = false;
						if (map->hexagons[last.y + 1][last.x - 1].getNationality() != map->hexagons[last.y][last.x].getNationality())
							foreignMovement = true;
						if (foreignMovement && typeid(*this).name() == "LogisticUnit")
							return;
						map->hexagons[last.y + 1][last.x - 1].wasAStarred = true;
						if (sf::Vector2i(last.x - 1, last.y + 1) == to)
						{
							road.clear();
							roads[shortestInd].push_back(sf::Vector2i(last.x - 1, last.y + 1));
							for (int i = int(roads[shortestInd].size()) - 1; i > 0; i--) // add road to main unit road
								road.push_back(roads[shortestInd][i]);
							break;
						}
						std::vector<sf::Vector2i> r = roads[shortestInd];
						r.push_back(sf::Vector2i(last.x - 1, last.y + 1));
						roads.push_back(r);
						float l;
						if (!foreignMovement)
							l = lengths[shortestInd] + 1 / map->hexagons[last.y + 1][last.x - 1].speedMultiplier;
						else
							l = lengths[shortestInd] + 1 / (map->hexagons[last.y + 1][last.x - 1].speedMultiplier* 0.5f);
						lengths.push_back(l);
					}
				}
			}
		}
		else if (last.x % 2 == 0)
		{
			if (last.y > 0)
			{
				if (last.x > 0)
				{
					if (map->hexagons[last.y - 1][last.x - 1].wasAStarred == false)
					{
						bool foreignMovement = false;
						if (map->hexagons[last.y - 1][last.x - 1].getNationality() != map->hexagons[last.y][last.x].getNationality())
							foreignMovement = true;
						if (foreignMovement && typeid(*this).name() == "LogisticUnit")
							return;
						map->hexagons[last.y - 1][last.x - 1].wasAStarred = true;
						if (sf::Vector2i(last.x - 1, last.y - 1) == to)
						{
							road.clear();
							roads[shortestInd].push_back(sf::Vector2i(last.x - 1, last.y - 1));
							for (int i = int(roads[shortestInd].size()) - 1; i > 0; i--) // add road to main unit road
								road.push_back(roads[shortestInd][i]);
							break;
						}
						std::vector<sf::Vector2i> r = roads[shortestInd];
						r.push_back(sf::Vector2i(last.x - 1, last.y - 1));
						roads.push_back(r);
						float l;
						if (!foreignMovement)
							l = lengths[shortestInd] + 1 / map->hexagons[last.y - 1][last.x - 1].speedMultiplier;
						else
							l = lengths[shortestInd] + 1 / (map->hexagons[last.y - 1][last.x - 1].speedMultiplier* 0.5f);
						lengths.push_back(l);
					}
				}
				if (last.x < map->getSize().x - 1)
				{
					if (map->hexagons[last.y - 1][last.x + 1].wasAStarred == false)
					{
						bool foreignMovement = false;
						if (map->hexagons[last.y - 1][last.x + 1].getNationality() != map->hexagons[last.y][last.x].getNationality())
							foreignMovement = true;
						if (foreignMovement && typeid(*this).name() == "LogisticUnit")
							return;
						map->hexagons[last.y - 1][last.x + 1].wasAStarred = true;
						if (sf::Vector2i(last.x + 1, last.y - 1) == to)
						{
							road.clear();
							roads[shortestInd].push_back(sf::Vector2i(last.x + 1, last.y - 1));
							for (int i = int(roads[shortestInd].size()) - 1; i > 0; i--) // add road to main unit road
								road.push_back(roads[shortestInd][i]);
							break;
						}
						std::vector<sf::Vector2i> r = roads[shortestInd];
						r.push_back(sf::Vector2i(last.x + 1, last.y - 1));
						roads.push_back(r);
						float l;
						if (!foreignMovement)
							l = lengths[shortestInd] + 1 / map->hexagons[last.y - 1][last.x + 1].speedMultiplier;
						else
							l = lengths[shortestInd] + 1 / (map->hexagons[last.y - 1][last.x + 1].speedMultiplier * 0.5f);
						lengths.push_back(l);
					}
				}
			}
		}
		///////////////////////////////////end of expansion
		//remove last road
		lengths.erase(lengths.begin() + shortestInd);
		roads.erase(roads.begin() + shortestInd);
	}

	// prepare hexagons for reuse
	for (int i = 0; i < roads.size(); i++)
		for (int j = 0; j < roads[i].size(); j++)
			map->hexagons[roads[i][j].y][roads[i][j].x].wasAStarred = false;
}