It's a game I created in 2018 trying to make simple hexagonal strategy using C++ as back-end.

I suspended project after vector structure (that was used to store units on certain fields) was giving me problems, throwing exceptions I could not wrap my head around. Anyway I managed to create something that may be worth showing.


![](images/surplus.jpg)


Moving units taking neighbouring hexagons

![](images/conquering.jpg)
